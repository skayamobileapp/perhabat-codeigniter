<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class TrainingCenterLogin extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('training_center_login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        // echo "<Pre>";print_r("dasdsa");exit();
        $this->checkTrainigCenterLoggedIn();
    }
    

    function checkTrainigCenterLoggedIn()
    {
        $isTrainingCenterLoggedIn = $this->session->userdata('isTrainingCenterLoggedIn');
        
        if(!isset($isTrainingCenterLoggedIn) || $isTrainingCenterLoggedIn != TRUE)
        {
            $this->load->view('training_center_login');
        }
        else
        {
            redirect('training_center/profile');
        }
    }

    public function loginMe()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->training_center_login_model->loginMe($email, $password);
            // echo "<Pre>"; print_r($result);exit;
            
            
            if(!empty($result))
            {                    
                $lastLogin = $this->training_center_login_model->trainingCenterLastLoginInfo($result->id);

                if($lastLogin == '')
                {
                    $training_center_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $training_center_login = $lastLogin->created_dt_tm;
                }

                $sessionArray = array(
                    'training_center_id'=>$result->id,
                    'training_center_name'=>$result->name,
                    'training_center_partner_id'=>$result->training_partner_id,
                    'training_center_email'=>$result->email,
                    'training_center_mobile'=>$result->mobile,
                    'training_center_status'=>$result->status,
                    'training_center_last_login'=> $training_center_login,
                    'isTrainingCenterLoggedIn' => TRUE
                    );
            
                // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);
                
                unset($sessionArray['training_center_id'], $sessionArray['isTrainingCenterLoggedIn'], $sessionArray['training_center_last_login']);

                $loginInfo = array("training_center_id"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_training_center_session_id", md5($uniqueId));


                $this->training_center_login_model->addTrainingCenterLastLogin($loginInfo);

                // echo "<Pre>"; print_r($this->session->userdata());exit();
                // echo md5($uniqueId);exit();
                redirect('/training_center/profile');
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                $this->index();
            }
        }
    }
}

?>