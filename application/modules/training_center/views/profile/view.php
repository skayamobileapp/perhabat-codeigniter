<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <!-- <h3>Student Profile</h3> -->
    </div>

     <h2 align="center">Training Center : Welcome <?php echo $training_center_name; ?></h2>


        <div class="form-container">
            <h4 class="form-group-title">Training Center Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Training Center Name :</dt>
                                <dd><?php echo ucwords($trainingCenter->name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Email :</dt>
                                <dd><?php echo $trainingCenter->email ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Mobile :</dt>
                                <dd><?php echo $trainingCenter->mobile ?></dd>
                            </dl>
                            <dl>
                                <dt>Status :</dt>
                                <dd><?php if($trainingCenter->status == 1)
                                {
                                  echo 'Active';
                                }
                                else
                                {
                                  echo 'Inactive';
                                }
                                ?>                                  
                                </dd>
                            </dl>                            
                        </div>
    
                    </div>
                </div>

        </div>




  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
</script>