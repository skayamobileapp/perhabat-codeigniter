<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Change Company User Password</h3>
            </div>



    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Company User Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">


            <div class="panel-body">




                <div class="form-container">
                  <h4 class="form-group-title">Company Details</h4>
                      

                      <div class="row">


                          <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Company Name <span class='error-text'>*</span></label>
                                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $companyDetails->name; ?>" readonly>
                                  </div>
                              </div>

                              
                          

                              <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Company Registration Number <span class='error-text'>*</span></label>
                                      <input type="text" class="form-control" id="registration_number" name="registration_number" value="<?php echo $companyDetails->registration_number; ?>" readonly>
                                  </div>
                              </div>



                              <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Joined Date <span class='error-text'>*</span></label>
                                      <input type="text" class="form-control datepicker" id="joined_date" name="joined_date" value="<?php echo $companyDetails->joined_date; ?>" readonly>
                                  </div>
                              </div>
                      </div>

                      <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Website <span class='error-text'>*</span></label>
                                  <input type="text" class="form-control" id="website" name="website" value="<?php echo $companyDetails->website; ?>" readonly>
                              </div>
                          </div>


                          <div class="col-sm-4">
                              <div class="form-group">
                                  <p>Status <span class='error-text'>*</span></p>
                                  <label class="radio-inline">
                                    <input type="radio" name="active_status" id="active_status1" value="1" checked="checked" disabled><span class="check-radio"></span> Active
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="active_status" id="active_status" value="0" disabled><span class="check-radio"></span> Inactive
                                  </label>
                              </div>
                          </div>

                      </div>


              </div>

              <br>



              <div class="form-container">
                   <h4 class="form-group-title">Company User Details</h4>

                   
                    <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Name <span class='error-text'>*</span></label>
                                  <input type="text" class="form-control" id="user_name1" name="user_name1" value="<?php echo $companyUser->name; ?>" readonly>
                              </div>
                          </div>

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Contact Number <span class='error-text'>*</span></label>
                                  <input type="number" class="form-control" id="user_number" name="user_number" value="<?php echo $companyUser->phone; ?>" readonly>
                              </div>
                          </div>


                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Email <span class='error-text'>*</span></label>
                                  <input type="text" class="form-control" id="email" name="email" value="<?php echo $companyUser->email; ?>" readonly>
                              </div>
                          </div>


                    </div>
                      
                  

                   <div class="row">
                   
                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Designation <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="user_designation" name="user_designation" value="<?php echo $companyUser->designation; ?>" readonly>
                          </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Role <span class='error-text'>*</span></label>
                              <select name="id_user_role" id="id_user_role" class="form-control" disabled>
                                  <option value="">Select</option>
                                  <?php
                                  if (!empty($companyUserRoleList))
                                  {
                                      foreach ($companyUserRoleList as $record)
                                      {?>
                                          <option value="<?php echo $record->id;?>"
                                      <?php
                                      if($companyUser->id_user_role == $record->id)
                                      {
                                          echo 'selected';
                                      }
                                      ?>
                                      ><?php echo $record->name;?>
                                          </option>
                                  <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>
                      </div>


                   </div>
                
              </div>




                



              
            </div>
          </div>
        </form>
      </div>
    </div>


    <br>


    <form id="form_programme" action="" method="post">
        
    
      <div class="form-container">
         <h4 class="form-group-title">Change Password Here</h4>

         <div class="row">
         
            <div class="col-sm-4">
               <div class="form-group">
                  <label>Enter Current Password <span class='error-text'>*</span></label>
                  <input type="password" class="form-control" id="old_password" name="old_password" onblur="checkCompanyUserPassword()">
               </div>
            </div>           
            
         </div>
      
      </div>





      <div class="button-block clearfix">
          <div class="bttn-group">
              <button type="button" class="btn btn-primary btn-lg" name="btn" id="btn" disabled="true" onclick="openModalDialogue()">Continue</button>
          </div>
      </div>


   



      <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>

                <form id="form_four" action="" method="post">


                  <div class="form-container">
                    <h4 class="form-group-title"> New Password Details</h4>

                    <div class="row">


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>New Password <span class='error-text'>*</span></label>
                                <input type="password" class="form-control" id="new_password" name="new_password" onblur="enableConfirmPassword()">
                            </div>
                        </div>


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Confirm Password <span class='error-text'>*</span></label>
                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" readonly>
                            </div>
                        </div>


                      </div>



                  </div>
              
              </form>

              <div class="modal-footer">
                  <button type="button" class="btn btn-default" onclick="updateCompanyUserPassword()">Update Password</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>



            </div>
            </div>

          </div>
      

      </div>


    </form>


        
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>


    </div>
</div>

<script>

    $('select').select2();




    $(function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });

    function openModalDialogue()
    {
      $('#myModal').modal('show');
    }


    function checkCompanyUserPassword()
    {
      if($('#form_programme').valid())
      {

        var old_password = $("#old_password").val();


        if(old_password != '')
        {

            var tempPR = {};
            tempPR['id_company_user'] = '<?php echo $id_company_user; ?>;'
            tempPR['id_company'] = '<?php echo $id_company; ?>;'
            tempPR['old_password'] = old_password;
            // alert(tempPR['id_program']);

                $.ajax(
                {
                   url: '/company_user/company/checkCompanyUserPassword',
                    type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                      // alert(result);
                      if(result == '0')
                      {
                          document.getElementById("btn").disabled = true;                            
                          alert('Check The Password Entered, Password Is Incorrect');
                          // $("#id_intake").val('');
                      }
                      else if(result == '1')
                      {
                          document.getElementById("btn").disabled = false;
                      }
                   }
                });
        }

      }
    }

    function enableConfirmPassword()
    {
        var new_password = $("#new_password").val();
        // alert(new_password);

        if(new_password != '')
        {
          document.getElementById("confirm_password").removeAttribute('readonly');
        }
    }

    function updateCompanyUserPassword()
    {
        var new_password = $("#new_password").val();
        var confirm_password = $("#confirm_password").val();
        // alert(confirm_password);

        if(new_password == confirm_password)
        {
          // changeCompanyUserPassword();
          $('#form_programme').submit();
        }
        else
        {
          alert("Entered Paswword's Doesn't Match. Try Again");
          $("#new_password").val('');
          $("#confirm_password").val('');
          document.getElementById("confirm_password").readOnly = true;
        }
    }

    function changeCompanyUserPassword()
    {
        // alert('change password');

        var tempPR = {};
        tempPR['id_company_user'] = <?php echo $id_company_user; ?>;
        tempPR['id_company'] = <?php echo $id_company; ?>;
        tempPR['new_password'] = new_password;
            // alert(tempPR['id_program']);

        $.ajax(
        {
           url: '/company_user/company/changeCompanyUserPassword',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              alert(result);
              
              alert('Company User Password Changed Succesfully');
              $('#form_programme').submit();
              // alert(result);
           }
        });
    }

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                old_password: {
                    required: true
                },
                new_password: {
                    required: true
                },
                confirm_password: {
                    required: true
                }
            },
            messages: {
                old_password: {
                    required: "<p class='error-text'>Old Password Required</p>",
                },
                new_password: {
                    required: "<p class='error-text'>New Password Required</p>",
                },
                confirm_password: {
                    required: "<p class='error-text'>Confirm Password Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>