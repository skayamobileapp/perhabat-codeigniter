<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends CI_Model
{
    function getTrainingCenter($id)
    {
        $this->db->select('p.*');
        $this->db->from('training_center as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}