<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Student extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
        $this->isTrainingCenterLoggedIn();
    }

    function list()
    {
        $training_center_id = $this->training_center_id;
        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['email'] = $this->security->xss_clean($this->input->post('email'));
        $formData['training_center_id'] = $training_center_id;
        $data['searchParam'] = $formData;

        $data['studentList'] = $this->student_model->studentListSearch($formData);
        $this->global['pageTitle'] = 'Perhebat : Student';
        //print_r($subjectDetails);exit;
        $this->loadViews("student/list", $this->global, $data, NULL);
    }
    
    function add()
    {
        $training_center_id = $this->session->training_center_id;
        $id_session = $this->session->my_training_center_session_id;

        if($this->input->post())
        {
            // echo '<Pre>';print_r($this->input->post());exit;
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $dob = $this->security->xss_clean($this->input->post('dob'));
            $father_name = $this->security->xss_clean($this->input->post('father_name'));
            $bank_name = $this->security->xss_clean($this->input->post('bank_name'));
            $account_number = $this->security->xss_clean($this->input->post('account_number'));
            $highest_education = $this->security->xss_clean($this->input->post('highest_education'));
            $year_of_passing = $this->security->xss_clean($this->input->post('year_of_passing'));
            $status = $this->security->xss_clean($this->input->post('status'));
            $address = $this->security->xss_clean($this->input->post('address'));
            $address_two = $this->security->xss_clean($this->input->post('address_two'));
            $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
            $country_id = $this->security->xss_clean($this->input->post('country_id'));
            $state_id = $this->security->xss_clean($this->input->post('state_id'));
            $job_role_id = $this->security->xss_clean($this->input->post('job_role_id'));
        
            $data = array(
                'full_name' => $first_name . " " . $last_name,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'contact_number' => $contact_number,
                'email' => $email,
                'password' => md5($email),
                'gender' => $gender,
                'dob' => date('Y-m-d H:i:s', strtotime($dob)),
                'father_name' => $father_name,
                'bank_name' => $bank_name,
                'account_number' => $account_number,
                'highest_education' => $highest_education,
                'year_of_passing' => $year_of_passing,
                'address' => $address,
                'address_two' => $address_two,
                'zipcode' => $zipcode,
                'country_id' => $country_id,
                'state_id' => $state_id,
                'training_center_id' => $training_center_id,
                'job_role_id' => $job_role_id,
                'status' => $status,
                'created_by' => 0
            );
            // echo '<Pre>';print_r($data);exit;
        
            $result = $this->student_model->addNewStudent($data);
            redirect('/training_center/student/list');
        }

        $data['trainingCenter'] = $this->student_model->getTrainingCenter($training_center_id);
        $data['countryList'] = $this->student_model->countryListByStatus('1');
        $data['jobRoleList'] = $this->student_model->getJobRolesByTrainingCenterId($training_center_id);

        $this->global['pageTitle'] = 'Perhebat : Add Training Partner';
        $this->loadViews("student/add", $this->global, $data, NULL);
    }

    function edit($id = NULL)
    {
        $training_center_id = $this->training_center_id;
        $id_session = $this->session->my_training_center_session_id;

        if ($id == null)
        {
            redirect('/setup/student/list');
        }
        if($this->input->post())
        {
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $dob = $this->security->xss_clean($this->input->post('dob'));
            $father_name = $this->security->xss_clean($this->input->post('father_name'));
            $bank_name = $this->security->xss_clean($this->input->post('bank_name'));
            $account_number = $this->security->xss_clean($this->input->post('account_number'));
            $highest_education = $this->security->xss_clean($this->input->post('highest_education'));
            $year_of_passing = $this->security->xss_clean($this->input->post('year_of_passing'));
            $status = $this->security->xss_clean($this->input->post('status'));
            $address = $this->security->xss_clean($this->input->post('address'));
            $address_two = $this->security->xss_clean($this->input->post('address_two'));
            $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
            $country_id = $this->security->xss_clean($this->input->post('country_id'));
            $state_id = $this->security->xss_clean($this->input->post('state_id'));
            $job_role_id = $this->security->xss_clean($this->input->post('job_role_id'));
        
            $data = array(
                'full_name' => $first_name . " " . $last_name,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'contact_number' => $contact_number,
                'email' => $email,
                // 'password' => md5($email),
                'gender' => $gender,
                'dob' => date('Y-m-d H:i:s', strtotime($dob)),
                'father_name' => $father_name,
                'bank_name' => $bank_name,
                'account_number' => $account_number,
                'highest_education' => $highest_education,
                'year_of_passing' => $year_of_passing,
                'address' => $address,
                'address_two' => $address_two,
                'zipcode' => $zipcode,
                'country_id' => $country_id,
                'state_id' => $state_id,
                'training_center_id' => $training_center_id,
                'job_role_id' => $job_role_id,
                'updated_by' => 0,
                'updated_dt_tm' => date('Y-m-d H:i:s')
            );

            $result = $this->student_model->editStudentDetails($data,$id);
            redirect('/training_center/student/list');
        }
        $data['trainingCenter'] = $this->student_model->getTrainingCenter($training_center_id);
        $data['student'] = $this->student_model->getStudent($id);
        $data['countryList'] = $this->student_model->countryListByStatus('1');
        $data['jobRoleList'] = $this->student_model->getJobRolesByTrainingCenterId($training_center_id);
        // print_r($data['trainingCenter']);exit;

        $this->global['pageTitle'] = 'Perhebat : Edit Student';
        $this->loadViews("student/edit", $this->global, $data, NULL);
    }

    function getStateByCountry($id_country)
    {
        $results = $this->student_model->getStateByCountryId($id_country);
        // print_r($results);exit;

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="<select name='state_id' id='state_id' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
        exit;

    }

    function getJobRolesByTrainingCenterId($training_center_id)
    {
        $results = $this->student_model->getJobRolesByTrainingCenterId($training_center_id);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="<select name='job_role_id' id='job_role_id' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
        exit;
    }
}
