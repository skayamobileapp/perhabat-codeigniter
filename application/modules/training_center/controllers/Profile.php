<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Profile extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('profile_model');
        $this->isTrainingCenterLoggedIn();
    }

    public function index()
    {
        $training_center_id = $this->training_center_id;
        $data['trainingCenter'] = $this->profile_model->getTrainingCenter($training_center_id);
        $this->global['pageTitle'] = 'Perhebat : Training Center';
        $this->loadViews("profile/view", $this->global, $data, NULL);
    }

    function logout()
    {   
        $sessionArray = array(
                'training_center_id'=> '',                    
                'training_center_name'=> '',
                'training_center_partner_id'=> '',
                'training_center_email'=> '',
                'training_center_mobile'=>  '',
                'training_center_status'=>  '',
                'training_center_last_login'=>  '',
                'isTrainingCenterLoggedIn' => FALSE
            );
        
        $this->session->set_userdata($sessionArray);
        $this->isTrainingCenterLoggedIn();
    }
}