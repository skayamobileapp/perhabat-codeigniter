<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Training_partner_model extends CI_Model
{
    function trainingPartnerListSearch($search)
    {
        $this->db->select('ay.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('training_partner as ay');
        $this->db->join('users as cre','ay.created_by = cre.id','left');
        $this->db->join('users as upd','ay.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getTrainingPartner($id)
    {
        $this->db->select('*');
        $this->db->from('training_partner');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewTrainingPartner($data)
    {
        $this->db->trans_start();
        $this->db->insert('training_partner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editTrainingPartnerDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('training_partner', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('training_partner');
        return $this->db->affected_rows();
    }
}

