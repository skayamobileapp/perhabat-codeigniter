<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Training_center_model extends CI_Model
{
    function trainingCenterListSearch($search)
    {
        $this->db->select('tc.*, tp.name as training_partner_name, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('training_center as tc');
        $this->db->join('training_partner as tp','tc.training_partner_id = tp.id','left');
        $this->db->join('users as cre','tc.created_by = cre.id','left');
        $this->db->join('users as upd','tc.updated_by = upd.id','left');
        if ($search['name'] != '')
        {
            $likeCriteria = "(tc.name  LIKE '%" . $search['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($search['email'] != '')
        {
            $likeCriteria = "(tc.email  LIKE '%" . $search['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($search['training_partner_id'] != '')
        {
            $this->db->where('tc.training_partner_id', $search['training_partner_id']);
        }

        $this->db->order_by("tc.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getTrainingCenter($id)
    {
        $this->db->select('*');
        $this->db->from('training_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewTrainingCenter($data)
    {
        $this->db->trans_start();
        $this->db->insert('training_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editTrainingCenterDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('training_center', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('training_center');
        return $this->db->affected_rows();
    }

    function trainingPartnerListByStatus($status)
    {
        $this->db->select('tc.*');
        $this->db->from('training_partner as tc');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function trainingPartnerList()
    {
        $this->db->select('tc.*');
        $this->db->from('training_partner as tc');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function userRolesListByStatus($status)
    {
        $this->db->select('tc.*');
        $this->db->from('job_roles as tc');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();   

        return $result;
    }

    function addTrainingCenterHasJobRoles($data)
    {
        $this->db->trans_start();
        $this->db->insert('training_center_has_job_roles', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function trainingCenterjobRolesListByTrainingCenterId($id)
    {
        $this->db->select('tcjr.*, tc.name as training_center_name, jr.name as job_role_name');
        $this->db->from('training_center_has_job_roles as tcjr');
        $this->db->join('training_center as tc','tcjr.training_center_id = tc.id','left');
        $this->db->join('job_roles as jr','tcjr.job_roles_id = jr.id','left');
        $this->db->where('tcjr.training_center_id', $id);
        $this->db->order_by("tcjr.id", "DESC");
        $query = $this->db->get();
        $result = $query->result();   

        return $result;
    }
}

