<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course_master_model extends CI_Model
{
        
    function courseList()
    {
        $this->db->select('c.*, d.name as department, s.name as coordinator_name');
        $this->db->from('course as c');
        $this->db->join('department as d', 'c.id_department = d.id');
        $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseListSearch($search)
    {
        $this->db->select('c.*, d.name as department, s.salutation, s.name as coordinator_name');
        $this->db->from('course as c');
        $this->db->join('department as d', 'c.id_department = d.id');
        $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($search))
        {
            $likeCriteria = "(c.name  LIKE '%" . $search . "%' or c.name_in_malay  LIKE '%" . $search . "%' or c.code  LIKE '%" . $search . "%' or s.name LIKE '%" . $search . "%' or d.name LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getCourse($id)
    {
         $this->db->select('c.*, d.name as department, s.name as coordinator_name');
        $this->db->from('course as c');
        $this->db->join('department as d', 'c.id_department = d.id');
        $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        // echo "<pre>";print_r($query);die;

        return $query->row();
    }

    function addCourseData($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_requisite', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getCourseData($id)
    {
        $this->db->select('cr.*, c.name as coursename, c.code as coursecode, gsd.description as min_pass_grade');
        $this->db->from('course_requisite as cr');
        $this->db->join('course as c', 'cr.id_course = c.id','left');
        $this->db->join('grade_setup_details as gsd', 'cr.min_pass_grade = gsd.id','left');
        $this->db->where('id_master_course', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getCourseRequisitData($id)
    {
        $this->db->select('cr.*, c.description as coursename');
        $this->db->from('course_requisite as cr');
        $this->db->join('grade_setup_details as c', 'cr.id_course = c.id','left');
        $this->db->where('id_master_course', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function addNewCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editCourse($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course', $data);

        return TRUE;
    }

    function deleteState($id, $courseInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('course', $courseInfo);

        return $this->db->affected_rows();
    }

    function deleteCourseData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('course_requisite');
         return TRUE;
    }

    function getGradeSetupDetailsByCourse($id_course)
    {
        $this->db->select('cr.*');
        $this->db->from('grade_setup as cr');
        $this->db->where('cr.id_course', $id_course);
        $this->db->where('cr.based_on', 'Program & Subject');
        $this->db->where('cr.status', '1');
        $this->db->order_by("cr.id", "DESC");
        $query = $this->db->get();
        $result = $query->row();

        if(!empty($result))
        {
            $id_master = $result->id;

            $this->db->select('cr.*');
            $this->db->from('grade_setup_details as cr');
            $this->db->where('cr.id_grade_setup', $id_master);
            $query = $this->db->get();
            $result = $query->result();

            return $result;
        }
        else
        {
            return array();
        }
    }
}
