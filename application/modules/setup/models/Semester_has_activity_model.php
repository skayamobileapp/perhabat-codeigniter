<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Semester_has_activity_model extends CI_Model
{
    function semesterHasActivityList()
    {
        $this->db->select('sha.*, s.name as semester, ad.name as activity');
        $this->db->from('semester_has_activity as sha');
        $this->db->join('semester as s', 'sha.id_semester = s.id');
        $this->db->join('activity_details as ad', 'sha.id_activity = ad.id');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function semesterHasActivityListSearch($formData)
    {
        $this->db->select('sha.*, s.name as semester, ad.name as activity');
        $this->db->from('semester_has_activity as sha');
        $this->db->join('semester as s', 'sha.id_semester = s.id');
        $this->db->join('activity_details as ad', 'sha.id_activity = ad.id');
        if($formData['id_semester']) {
            $likeCriteria = "(sha.id_semester  LIKE '%" . $formData['id_semester'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['id_activity']) {
            $likeCriteria = "(sha.id_activity  LIKE '%" . $formData['id_activity'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getSemesterHasActivity($id)
    {
    	$this->db->select('sha.*, s.name as semester, ad.name as activity');
        $this->db->from('semester_has_activity as sha');
        $this->db->join('semester as s', 'sha.id_semester = s.id');
        $this->db->join('activity_details as ad', 'sha.id_activity = ad.id');
        $this->db->where('sha.id', $id);

        $query = $this->db->get();
        // echo "<Pre>";print_r($query->row());exit;
        return $query->row();
    }
    
    function addNewSemesterHasActivity($data)
    {
        $this->db->trans_start();
        $this->db->insert('semester_has_activity', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSemesterHasActivity($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('semester_has_activity', $data);
        return TRUE;
    }
}