<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Job_roles_model extends CI_Model
{
    function jobRolesListSearch($search)
    {
        $this->db->select('ay.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('job_roles as ay');
        $this->db->join('users as cre','ay.created_by = cre.id','left');
        $this->db->join('users as upd','ay.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getJobRoles($id)
    {
        $this->db->select('*');
        $this->db->from('job_roles');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewJobRoles($data)
    {
        $this->db->trans_start();
        $this->db->insert('job_roles', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editJobRolesDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('job_roles', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('job_roles');
        return $this->db->affected_rows();
    }
}

