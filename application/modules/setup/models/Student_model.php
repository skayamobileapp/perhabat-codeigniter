<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_model extends CI_Model
{
    function studentListSearch($search)
    {
        $this->db->select('ay.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('student as ay');
        $this->db->join('users as cre','ay.created_by = cre.id','left');
        $this->db->join('users as upd','ay.updated_by = upd.id','left');
        if ($search['name'] != '')
        {
            $likeCriteria = "(ay.full_name  LIKE '%" . $search['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($search['email'] != '')
        {
            $likeCriteria = "(ay.email  LIKE '%" . $search['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($search['training_center_id'] != '')
        {
            $this->db->where('ay.training_center_id', $search['training_center_id']);
        }
        $this->db->order_by("ay.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewStudent($data)
    {
        $this->db->trans_start();
        $this->db->insert('student', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStudentDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('student');
        return $this->db->affected_rows();
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getJobRolesByTrainingCenterId($training_center_id)
    {
        $this->db->select('DISTINCT(tcjr.job_roles_id) as id,  jr.name');
        $this->db->from('training_center_has_job_roles as tcjr');
        $this->db->join('job_roles as jr','tcjr.job_roles_id = jr.id','left');
        $this->db->where('jr.status', '1');
        $this->db->where('tcjr.training_center_id', $training_center_id);
        // $this->db->order_by("jr.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function trainingCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('training_center');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function trainingCenterList()
    {
        $this->db->select('*');
        $this->db->from('training_center');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    
}

