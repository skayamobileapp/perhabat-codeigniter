<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentPending extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_pending_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('student.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['job_roles_id'] = $this->security->xss_clean($this->input->post('job_roles_id'));
            $formData['training_center_id'] = $this->security->xss_clean($this->input->post('training_center_id'));
            $formData['status'] = 1;
            $data['searchParam'] = $formData;

            $data['trainingCenterList'] = $this->student_pending_model->trainingCenterList();
            $data['jobRolesList'] = $this->student_pending_model->jobRolesList();
            $data['trainingCenterjobRolesList'] = $this->student_pending_model->trainingCenterjobRolesListSearch($formData);

            $this->global['pageTitle'] = 'Perhebat : Training Partner';
            //print_r($subjectDetails);exit;
            $this->loadViews("student_pending/list", $this->global, $data, NULL);
        }
    }

    function studentList($id = null)
    {
        if ($this->checkAccess('student.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $trainingCenterHasJobRole = $this->student_pending_model->getTrainingCenterHasJobRole($id);
            if(!empty($trainingCenterHasJobRole))
            {

                $data['trainingCenterHasJobRole'] = $trainingCenterHasJobRole;
                $formData['name'] = $this->security->xss_clean($this->input->post('name'));
                $formData['email'] = $this->security->xss_clean($this->input->post('email'));
                $formData['training_center_id'] = $trainingCenterHasJobRole->training_center_id;
                $formData['job_role_id'] = $trainingCenterHasJobRole->job_roles_id;
                $formData['status'] = 'Pending';
                $data['searchParam'] = $formData;

                $data['trainingCenter'] = $this->student_pending_model->getTrainingCenter($trainingCenterHasJobRole->training_center_id);
                $data['jobRole'] = $this->student_pending_model->getTrainingCenter($trainingCenterHasJobRole->job_roles_id);

                $data['trainingCenterList'] = $this->student_pending_model->trainingCenterList();
                $data['studentList'] = $this->student_pending_model->studentListSearch($formData);
                $this->global['pageTitle'] = 'Perhebat : Student List';
                //print_r($subjectDetails);exit;
                $this->loadViews("student_pending/student_list", $this->global, $data, NULL);

            }
        }
    }

    function edit($id = NULL, $training_center_has_job_role_id = NULL)
    {
        if ($this->checkAccess('student.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/StudentPending/list');
            }
            if($this->input->post())
            {
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $dob = $this->security->xss_clean($this->input->post('dob'));
                $father_name = $this->security->xss_clean($this->input->post('father_name'));
                $bank_name = $this->security->xss_clean($this->input->post('bank_name'));
                $account_number = $this->security->xss_clean($this->input->post('account_number'));
                $highest_education = $this->security->xss_clean($this->input->post('highest_education'));
                $year_of_passing = $this->security->xss_clean($this->input->post('year_of_passing'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $address_two = $this->security->xss_clean($this->input->post('address_two'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $country_id = $this->security->xss_clean($this->input->post('country_id'));
                $state_id = $this->security->xss_clean($this->input->post('state_id'));
                $training_center_id = $this->security->xss_clean($this->input->post('training_center_id'));
                $job_role_id = $this->security->xss_clean($this->input->post('job_role_id'));
            
                $data = array(
                    'full_name' => $first_name . " " . $last_name,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'contact_number' => $contact_number,
                    'email' => $email,
                    // 'password' => md5($email),
                    'gender' => $gender,
                    'dob' => date('Y-m-d H:i:s', strtotime($dob)),
                    'father_name' => $father_name,
                    'bank_name' => $bank_name,
                    'account_number' => $account_number,
                    'highest_education' => $highest_education,
                    'year_of_passing' => $year_of_passing,
                    'address' => $address,
                    'address_two' => $address_two,
                    'zipcode' => $zipcode,
                    'country_id' => $country_id,
                    'state_id' => $state_id,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->student_pending_model->editStudentDetails($data,$id);
                redirect('/setup/studentPending/studentList/'. $training_center_has_job_role_id);
            }

            $trainingCenterHasJobRole = $this->student_pending_model->getTrainingCenterHasJobRole($training_center_has_job_role_id);
            $data['trainingCenter'] = $this->student_pending_model->getTrainingCenter($trainingCenterHasJobRole->training_center_id);
            $data['jobRole'] = $this->student_pending_model->getTrainingCenter($trainingCenterHasJobRole->job_roles_id);

            $data['student'] = $this->student_pending_model->getStudent($id);
            $data['countryList'] = $this->student_pending_model->countryListByStatus('1');
            $data['trainingCenterList'] = $this->student_pending_model->trainingCenterListByStatus('1');
            $data['training_center_has_job_role_id'] = $training_center_has_job_role_id;

            $this->global['pageTitle'] = 'Perhebat : Edit Student';
            $this->loadViews("student_pending/edit", $this->global, $data, NULL);
        }
    }
}