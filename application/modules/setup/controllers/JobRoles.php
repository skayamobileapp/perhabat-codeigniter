<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class JobRoles extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('job_roles_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('job_roles.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $name;
            $data['jobRolesList'] = $this->job_roles_model->jobRolesListSearch($name);
            $this->global['pageTitle'] = 'Perhebat : Training Partner';
            //print_r($subjectDetails);exit;
            $this->loadViews("job_roles/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('job_roles.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $id_user
                );
            
                $result = $this->job_roles_model->addNewJobRoles($data);
                redirect('/setup/jobRoles/list');
            }

            $this->global['pageTitle'] = 'Perhebat : Add Training Partner';
            $this->loadViews("job_roles/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('job_roles.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/jobRoles/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->job_roles_model->editJobRolesDetails($data,$id);
                redirect('/setup/jobRoles/list');
            }
            $data['jobRoles'] = $this->job_roles_model->getJobRoles($id);
            $this->global['pageTitle'] = 'Perhebat : Edit Training Partner';
            $this->loadViews("job_roles/edit", $this->global, $data, NULL);
        }
    }
}
