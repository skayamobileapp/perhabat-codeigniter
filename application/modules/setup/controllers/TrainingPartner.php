<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TrainingPartner extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('training_partner_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('training_partner.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $name;
            $data['trainingPartnerList'] = $this->training_partner_model->trainingPartnerListSearch($name);
            $this->global['pageTitle'] = 'Perhebat : Training Partner';
            //print_r($subjectDetails);exit;
            $this->loadViews("training_partner/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('training_partner.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $contact_person = $this->security->xss_clean($this->input->post('contact_person'));
                $serial_number = $this->security->xss_clean($this->input->post('serial_number'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'contact_person' => $contact_person,
                    'serial_number' => $serial_number,
                    'status' => $status,
                    'created_by' => $id_user
                );
            
                $result = $this->training_partner_model->addNewTrainingPartner($data);
                redirect('/setup/trainingPartner/list');
            }

            $this->global['pageTitle'] = 'Perhebat : Add Training Partner';
            $this->loadViews("training_partner/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('training_partner.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/trainingPartner/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $contact_person = $this->security->xss_clean($this->input->post('contact_person'));
                $serial_number = $this->security->xss_clean($this->input->post('serial_number'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'contact_person' => $contact_person,
                    'serial_number' => $serial_number,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->training_partner_model->editTrainingPartnerDetails($data,$id);
                redirect('/setup/trainingPartner/list');
            }
            $data['trainingPartner'] = $this->training_partner_model->getTrainingPartner($id);
            $this->global['pageTitle'] = 'Perhebat : Edit Training Partner';
            $this->loadViews("training_partner/edit", $this->global, $data, NULL);
        }
    }
}
