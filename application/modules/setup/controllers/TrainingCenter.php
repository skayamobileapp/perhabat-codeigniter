<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TrainingCenter extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('training_center_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('training_center.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['training_partner_id'] = $this->security->xss_clean($this->input->post('training_partner_id'));
            $data['searchParam'] = $formData;

            $data['trainingCenterList'] = $this->training_center_model->trainingCenterListSearch($formData);
            $data['trainingPartnerList'] = $this->training_center_model->trainingPartnerList();

            $this->global['pageTitle'] = 'Perhebat : Training Center';
            $this->loadViews("training_center/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('training_center.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $training_partner_id = $this->security->xss_clean($this->input->post('training_partner_id'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'training_partner_id' => $training_partner_id,
                    'password' => md5($password),
                    'mobile' => $mobile,
                    'status' => $status,
                    'created_by' => $id_user
                );
            
                $result = $this->training_center_model->addNewTrainingCenter($data);
                redirect('/setup/trainingCenter/list');
            }

            $data['trainingPartnerList'] = $this->training_center_model->trainingPartnerListByStatus('1');

            $this->global['pageTitle'] = 'Perhebat : Add Training Center';
            $this->loadViews("training_center/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('training_center.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/trainingCenter/list');
            }
            if($this->input->post())
            {
                // echo '<Pre>';print_r($this->input->post());exit;
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                // $password = $this->security->xss_clean($this->input->post('password'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $training_partner_id = $this->security->xss_clean($this->input->post('training_partner_id'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'training_partner_id' => $training_partner_id,
                    // 'password' => md5($password),
                    'mobile' => $mobile,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->training_center_model->editTrainingCenterDetails($data,$id);
                redirect('/setup/trainingCenter/list');
            }

            $data['trainingPartnerList'] = $this->training_center_model->trainingPartnerListByStatus('1');
            $data['trainingCenter'] = $this->training_center_model->getTrainingCenter($id);
            $this->global['pageTitle'] = 'Perhebat : Edit Training Center';
            $this->loadViews("training_center/edit", $this->global, $data, NULL);
        }
    }

    function jobRoles($id = NULL)
    {
        if ($this->checkAccess('training_center.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/trainingCenter/list');
            }
            if($this->input->post())
            {
                // echo '<Pre>';print_r($this->input->post());exit;
                $job_roles_id = $this->security->xss_clean($this->input->post('job_roles_id'));
                $training_center_id = $this->security->xss_clean($this->input->post('training_center_id'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'job_roles_id' => $job_roles_id,
                    'training_center_id' => $training_center_id,
                    'status' => $status,
                    'created_by' => $id_user
                );
                // echo '<Pre>';print_r($data);exit;

                $result = $this->training_center_model->addTrainingCenterHasJobRoles($data);
                redirect('/setup/trainingCenter/jobRoles/'.$id);
            }
            $data['jobRolesList'] = $this->training_center_model->userRolesListByStatus('1');
            $data['trainingCenterjobRolesList'] = $this->training_center_model->trainingCenterjobRolesListByTrainingCenterId($id);
            $data['trainingCenter'] = $this->training_center_model->getTrainingCenter($id);
            // echo '<Pre>';print_r($data['trainingCenterjobRolesList']);exit;
            $this->global['pageTitle'] = 'Perhebat : Job Roles Training Center';
            $this->loadViews("training_center/job_roles", $this->global, $data, NULL);
        }
    }
}
