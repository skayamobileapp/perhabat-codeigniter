<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Welcome extends BaseController
{
	 public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('role_model');

    }

    public function index()
    {
        $id_role = $this->session->role;
        $data['id_role'] = $id_role;
        $data['student_count'] = "0";
        $data['employee_count'] = "0";
        $data['company_active_count'] = "0";
        $data['programme_active_count'] = "0";
        $this->global['pageTitle'] = 'Perhebat : Welcome To Setup';
        
        $this->loadViews("includes/welcome", $this->global, $data , NULL);
    }   
}