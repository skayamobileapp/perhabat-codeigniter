<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Intake</h3>
        </div>
        <form id="form_intake" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Intake Details</h4>

                <div class="row">

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Year <span class='error-text'>*</span></label>
                            <select class="form-control" id="year" name="year">

                                 <option value="">Select</option>
                            

                            <?php
                            if (!empty($yearList))
                            {
                                foreach ($yearList as $record)
                                {?>
                                     <option value="<?php echo $record->name;?>" <?php if((int)$intakeDetails->year==(int)$record->name) { echo "selected=selected"; }?> ><?php echo $record->name;?></option>


                                
                            <?php
                                }
                            }
                            ?>



                               
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name"  value="<?php echo $intakeDetails->name; ?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name In Other Language</label>
                            <input type="text" class="form-control " id="name_in_malay" name="name_in_malay" value="<?php echo $intakeDetails->name_in_malay; ?>">
                        </div>
                    </div>

                  
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Semester Sequence <span class='error-text'>*</span></label>
                            <select class="form-control" id="semester_sequence" name="semester_sequence">
                            <option value='January' <?php if($intakeDetails->semester_sequence=='January') { echo "selected=selected"; }?> >January</option>
                            <option value='February' <?php if($intakeDetails->semester_sequence=='February') { echo "selected=selected"; }?> >February</option>
                            <option value='March' <?php if($intakeDetails->semester_sequence=='March') { echo "selected=selected"; }?> >March</option>
                            <option value='April' <?php if($intakeDetails->semester_sequence=='April') { echo "selected=selected"; }?> >April</option>
                            <option value='May' <?php if($intakeDetails->semester_sequence=='May') { echo "selected=selected"; }?> >May</option>
                            <option value='June' <?php if($intakeDetails->semester_sequence=='June') { echo "selected=selected"; }?> >June</option>
                            <option value='July' <?php if($intakeDetails->semester_sequence=='July') { echo "selected=selected"; }?> >July</option>
                            <option value='August' <?php if($intakeDetails->semester_sequence=='August') { echo "selected=selected"; }?> >August</option>
                            <option value='September' <?php if($intakeDetails->semester_sequence=='September') { echo "selected=selected"; }?> >September</option>
                            <option value='October' <?php if($intakeDetails->semester_sequence=='October') { echo "selected=selected"; }?> >October</option>
                            <option value='November' <?php if($intakeDetails->semester_sequence=='November') { echo "selected=selected"; }?> >November</option>
                            <option value='December' <?php if($intakeDetails->semester_sequence=='December') { echo "selected=selected"; }?> >December</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Application Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d/m/Y',strtotime($intakeDetails->start_date)); ?>" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Application End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d/m/Y',strtotime($intakeDetails->end_date)); ?>" autocomplete="off">
                        </div>
                    </div>

                </div>


              


                <div class="row">

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Generate Conditional Offer Letter <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_temp_offer_letter" id="is_temp_offer_letter" value="0" <?php if($intakeDetails->is_temp_offer_letter=='0') {
                                     echo "checked=checked";
                                  };?>><span class="check-radio"></span> No
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_temp_offer_letter" id="is_temp_offer_letter" value="1" <?php if($intakeDetails->is_temp_offer_letter=='1') {
                                     echo "checked=checked";
                                  };?>>
                                  <span class="check-radio"></span> Yes
                                </label>                              
                            </div>                         
                    </div>


                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="1" <?php if($intakeDetails->status=='1') {
                                     echo "checked=checked";
                                  };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="0" <?php if($intakeDetails->status=='0') {
                                     echo "checked=checked";
                                  };?>>
                                  <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>

                
                </div>

            </div>

        </form>


        <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
        </div>



        




    <div class="form-container">
            <h4 class="form-group-title"> Intake Programme Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Program Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Registation Location</a>
                    </li>
                    <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Scheme Details</a>
                    </li>

                      
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_programme_intake" action="" method="post">
                            <div class="form-container">
                                <h4 class="form-group-title">Intake Has Programme Details</h4>


                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Select Program <span class='error-text'>*</span></label>
                                            <select name="id_programme" id="id_programme" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programmeList))
                                                {
                                                    foreach ($programmeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                  
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                    </div>

                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>
                        </form>


                        <?php

                            if(!empty($getIntakeHasProgramme))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Program Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                <th>Programme Name</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getIntakeHasProgramme);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getIntakeHasProgramme[$i]->program_code . " - " . $getIntakeHasProgramme[$i]->programme;?></td>
                                                <td>
                                                <a onclick="deleteIntakeHasProgramme(<?php echo $getIntakeHasProgramme[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">




                        <form id="form_branch" action="" method="post">
                            <div class="form-container">
                                <h4 class="form-group-title">Registation Location Details</h4>


                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Branch <span class='error-text'>*</span></label>
                                            <select name="id_branch" id="id_branch" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($branchList))
                                                {
                                                    foreach ($branchList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->name . " - " . $record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Registration Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="registration_date" name="registration_date" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Registration Time <span class='error-text'>*</span></label>
                                            <input type="time" class="form-control" id="registration_time" name="registration_time" autocomplete="off">
                                        </div>
                                    </div>


                                  
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveBranchData()">Add</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>
                        </form>


                        <?php

                            if(!empty($getIntakeHasBranch))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Intake Branch Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Branch</th>
                                                 <th>Registration Date</th>
                                                 <th>Registration Time</th>
                                                 <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getIntakeHasBranch);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getIntakeHasBranch[$i]->branch_code . " - " . $getIntakeHasBranch[$i]->branch_name;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($getIntakeHasBranch[$i]->registration_date));?></td>
                                                <td><?php echo $getIntakeHasBranch[$i]->registration_time;?></td>
                                                <td class="text-center">
                                                <a onclick="deleteIntakeTrainingCenter(<?php echo $getIntakeHasBranch[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_three">
                        <div class="col-12 mt-4">




                        <form id="form_scheme_nationality" action="" method="post">
                            <div class="form-container">
                                <h4 class="form-group-title">Scheme Details</h4>


                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Scheme <span class='error-text'>*</span></label>
                                            <select name="id_scheme_detail" id="id_scheme_detail" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($schemeList))
                                                {
                                                    foreach ($schemeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->description; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                  
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addIntakeHasScheme()">Add</button>
                                    </div>

                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>
                        </form>


                        <?php

                            if(!empty($intakeHasScheme))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Scheme Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                <th>Scheme</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($intakeHasScheme);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $intakeHasScheme[$i]->scheme_code . " - " . $intakeHasScheme[$i]->scheme_name;?></td>
                                                <td>
                                                <a onclick="deleteIntakeHasScheme(<?php echo $intakeHasScheme[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>


                </div>

            </div>
        

    </div> 


         

           

      </div>
    </div>

   </div> <!-- END row-->

           
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    function saveData()
    {

        if($('#form_programme_intake').valid())
        {


        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id'] = <?php echo $idIntake;?>;
            $.ajax(
            {
               url: '/setup/intake/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
                // $("#view").html(result);
               }
            });
        }
    }

    function deleteIntakeHasProgramme(id) {
                    // alert(id);

            $.ajax(
            {
               url: '/setup/intake/deleteIntakeHasProgramme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    function saveBranchData()
    {
        if($('#form_branch').valid())
        {

        var tempPR = {};
        tempPR['id_branch'] = $("#id_branch").val();
        tempPR['registration_date'] = $("#registration_date").val();
        tempPR['registration_time'] = $("#registration_time").val();
        tempPR['id_intake'] = <?php echo $idIntake;?>;
            $.ajax(
            {
               url: '/setup/intake/addIntakeHasBranch',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            });
        }
    }

    function deleteIntakeTrainingCenter(id) {
                    // alert(id);

            $.ajax(
            {
               url: '/setup/intake/deleteIntakeTrainingCenter/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    function validateDetailsData()
    {
        
                $('#form_intake').submit();
            
    }


    function addIntakeHasScheme()
    {
        if($('#form_scheme_nationality').valid())
        {

        var tempPR = {};
        tempPR['id_scheme'] = $("#id_scheme_detail").val();
        tempPR['id_intake'] = <?php echo $intakeDetails->id; ?>;
            $.ajax(
            {
               url: '/setup/intake/addIntakeHasScheme',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                window.location.reload();
                // $("#view").html(result);
               }
            });
        }
    }

    function deleteIntakeHasScheme(id)
    {
        // alert(id);
            $.ajax(
            {
               url: '/setup/intake/deleteIntakeHasScheme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    

    $(document).ready(function()
    {
        $("#form_scheme_nationality").validate({
            rules: {
                id_scheme_detail: {
                    required: true
                }
            },
            messages: {
                id_scheme_detail: {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_programme: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


     $(document).ready(function() {
        $("#form_branch").validate({
            rules: {
                id_branch: {
                    required: true
                },
                registration_date: {
                    required: true
                },
                registration_time: {
                    required: true
                }
            },
            messages: {
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                },
                registration_date: {
                    required: "<p class='error-text'>Select Registration Date</p>",
                },
                registration_time: {
                    required: "<p class='error-text'>Select Registration Time</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });






    $(document).ready(function() {
        // saveData();
        $("#form_intake").validate({
            rules: {
                name: {
                    required: true
                },
                year: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year required</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>