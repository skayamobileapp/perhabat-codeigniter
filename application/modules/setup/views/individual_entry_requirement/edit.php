<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Edit Program</h3>
            </div>


    <form id="form_programme" action="" method="post">
        
    <div class="form-container">
        <h4 class="form-group-title">Program Details</h4>
            

            <div class="row">

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $programmeDetails->code; ?>">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeDetails->name; ?>">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name Other Language</label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programmeDetails->name_optional_language; ?>">
                    </div>
                </div>

                
            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Award <span class='error-text'>*</span></label>
                        <select name="id_award" id="id_award" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($awardList))
                            {
                                foreach ($awardList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeDetails->id_award)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y', strtotime($programmeDetails->start_date)); ?>" autocomplete="off">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y', strtotime($programmeDetails->end_date)); ?>" autocomplete="off">
                    </div>
                </div>


            </div>


            <div class="row">



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($programmeDetails->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($programmeDetails->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>
    


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


        </form>

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    


    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                graduate_studies: {
                    required: true
                },
                foundation: {
                    required: true
                },
                total_cr_hrs: {
                    required: true
                },
                id_award: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Program Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Program Code Required</p>",
                },
                graduate_studies: {
                    required: "<p class='error-text'>Graduate Studies Required</p>",
                },
                foundation: {
                    required: "<p class='error-text'>Foundation Required</p>",
                },
                total_cr_hrs: {
                    required: "<p class='error-text'>Total Credit Hours required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                status: {
                    required: "<p class='error-text'>Select status</p>",
                },
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>