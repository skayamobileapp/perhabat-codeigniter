<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Training Center</h3>
        </div>


        <ul class="page-nav-links">
            <li class="active"><a href="/setup/trainingCenter/edit/<?php echo $trainingCenter->id;?>">Training Center</a></li>
            <li><a href="/setup/trainingCenter/jobRoles/<?php echo $trainingCenter->id;?>">Job Roles</a></li>
        </ul>

        <form id="form_academic_year" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Training Center Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Training Center Name <span class='error-text'>*</span></label>
                        <br>
                        <input type="text" class="form-control"  id="name" name="name" autocomplete="off" style="width: 360px; height: 30px" value="<?php echo $trainingCenter->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $trainingCenter->email;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Password <span class='error-text'>*</span></label>
                        <input type="password" class="form-control" id="password" name="password" autocomplete="off" value="<?php echo $trainingCenter->password;?>" readonly>
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mobile <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="mobile" name="mobile" autocomplete="off" value="<?php echo $trainingCenter->mobile;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Training Partner *</label>
                        <select name="training_partner_id" id="training_partner_id" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($trainingPartnerList))
                            {
                                foreach ($trainingPartnerList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $trainingCenter->training_partner_id)
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($trainingCenter->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($trainingCenter->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


   $(document).ready(function() {
        $("#form_academic_year").validate({
            rules: {
                name: {
                    required: true
                },
                email:
                {
                    required: true
                },
                contact_person:
                {
                    required: true
                },
                training_partner_id:
                {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                contact_person:
                {
                    required: "<p class='error-text'>Contact person Required</p>",
                },
                training_partner_id:
                {
                    required: "<p class='error-text'>Select Training partner</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>