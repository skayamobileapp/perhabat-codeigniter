<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Training Center</h3>
        </div>

        <ul class="page-nav-links">
            <li><a href="/setup/trainingCenter/edit/<?php echo $trainingCenter->id;?>">Trainng Center</a></li>
            <li class="active"><a href="/setup/trainingCenter/jobRoles/<?php echo $trainingCenter->id;?>">Job Roles</a></li>
        </ul>

        <form id="form_academic_year" action="" method="post">

        <input type="hidden" class="form-control" id="training_center_id" name="training_center_id" autocomplete="off" value="<?php echo $trainingCenter->id;?>">
        
        <div class="form-container">
            <h4 class="form-group-title">Job Roles Details</h4>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select User Roles *</label>
                        <select name="job_roles_id" id="job_roles_id" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($jobRolesList))
                            {
                                foreach ($jobRolesList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($trainingCenter->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($trainingCenter->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>



        <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Training Center</th>
            <th>Job Roles</th>
            <th style="text-align: center;">Status</th>
            <!-- <th style="text-align: center;">Action</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($trainingCenterjobRolesList)) {
            $i=1;
            foreach ($trainingCenterjobRolesList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                  <td><?php echo $record->training_center_name ?></td>
                  <td><?php echo $record->job_role_name ?></td>
                  <td style="text-align: center;"><?php if( $record->status == '1')
                  {
                    echo "Active";
                  }
                  else
                  {
                    echo "In-Active";
                  } 
                  ?></td>
                  <!-- <td class="text-center">
                      <a href="<?php echo 'edit/'.$record->id; ?>" title="Edit">Edit</a>
                  </td> -->
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


   $(document).ready(function() {
        $("#form_academic_year").validate({
            rules: {
                job_roles_id: {
                    required: true
                }
            },
            messages: {
                job_roles_id: {
                    required: "<p class='error-text'>Select Job Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>