<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Training Partner</h3>
        </div>
        <form id="form_academic_year" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Training Partner Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Training Partner Name <span class='error-text'>*</span></label>
                        <br>
                        <input type="text" class="form-control"  id="name" name="name" autocomplete="off" style="width: 360px; height: 30px">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_person" name="contact_person" autocomplete="off">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Serial Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="serial_number" name="serial_number" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    $(document).ready(function() {
        $("#form_academic_year").validate({
            rules: {
                name: {
                    required: true
                },
                email:
                {
                    required: true
                },
                contact_person:
                {
                    required: true
                },
                serial_number:
                {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                contact_person:
                {
                    required: "<p class='error-text'>Contact person Required</p>",
                },
                serial_number:
                {
                    required: "<p class='error-text'>Serial Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
