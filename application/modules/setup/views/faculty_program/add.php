<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Faculty Program</h3>
        </div>
        <form id="form_department" action="" method="post">

        <div class="form-container">
        <h4 class="form-group-title">Faculty Program Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control" onchange="typeShow(this.value)">
                            <option value="">Select</option>
                            <option value="Internal">Internal</option>
                            <option value="External">External</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4" id="show_partner_university">
                    <div class="form-group">
                        <label>Partner University <span class='error-text'>*</span></label>
                        <select name="id_partner_university" id="id_partner_university" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($partnerUniversityList))
                            {
                                foreach ($partnerUniversityList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>


     $('select').select2();

     typeShow();


    function typeShow(type)
    {
        if(type=='External')
        {
            $('#show_partner_university').show();
        }
        else
        {
            $('#show_partner_university').hide();
        }
    }


    $(document).ready(function(){
        $("#form_department").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                code:
                {
                    required: true
                },
                description:
                {
                    required: true
                },
                id_partner_university:
                {
                    required: true
                },
                type:
                {
                    required: true
                }                
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                description:
                {
                    required: "<p class='error-text'>Description Required</p>",
                },
                id_partner_university:
                {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                type:
                {
                    required: "<p class='error-text'>Select Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>