<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Training Center Has Job Roles Approved</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Student</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Job Roles</label>
                      <div class="col-sm-8">
                        <select name="job_roles_id" id="job_roles_id" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($jobRolesList))
                            {
                                foreach ($jobRolesList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $searchParam['job_roles_id'])
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Training Center</label>
                      <div class="col-sm-8">
                        <select name="training_center_id" id="training_center_id" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($trainingCenterList))
                            {
                                foreach ($trainingCenterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $searchParam['training_center_id'])
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    



    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Training Center Name</th>
            <th>Job Role Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Training Partner</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($trainingCenterjobRolesList)) {
            $i=1;
            foreach ($trainingCenterjobRolesList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->training_center_name ?></td>
                <td><?php echo $record->job_role_name ?></td>
                <td><?php echo $record->training_center_email ?></td>
                <td><?php echo $record->training_center_mobile ?></td>
                <td><?php echo $record->training_partner_name ?></td>
                <td><?php if( $record->training_partner_status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'studentList/' . $record->id; ?>" title="Edit">View</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>




  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();

      function clearSearchForm()
      {
        window.location.reload();
      }
</script>