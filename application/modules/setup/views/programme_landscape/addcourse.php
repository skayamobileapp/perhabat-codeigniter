<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Subject Registration</h3>
        </div>

        <form id="form_programme_landscape" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Programme Landscape Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Name  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeLandscapeDetails->name; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program  <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_programme)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" disabled="disabled" class="form-control">

                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $id_intake)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->year . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Type  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="program_landscape_type" name="program_landscape_type" value="<?php echo $programmeLandscapeDetails->program_landscape_type; ?>" readonly>
                    </div>
                </div>                

            </div>

        </div>

        <div class="form-container">
        <h4 class="form-group-title">Course Details</h4>

            <div class="row">

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Semester *</label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Type <span class='error-text'>*</span></label>
                        <select name="course_type" id="course_type" class="form-control" onchange="showProgramDetails()">


                              <?php
                            if (!empty($courseTypeList))
                            {
                                foreach ($courseTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course  <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label><?php echo $programmeLandscapeDetails->program_landscape_type; ?> <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php


                                for($i=1;$i<=$programmeLandscapeDetails->total_semester;$i++)
                                {?>
                                    <option value="<?php echo $i;?>">
                                        <?php echo $i;  ?>
                                    </option>
                            <?php
                                
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <p>Pre Requisite <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="pre_requisite" id="pre_requisite" value="Yes"><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="pre_requisite" id="pre_requisite" value="No" >
                          <span class="check-radio"></span> No
                        </label>                              
                    </div>                       
                </div> -->


                
            </div>

            <div class="row">

                <div class="col-sm-4" id="view_major" style="display: none;">
                    <div class="form-group">
                        <label>Program Major <span class='error-text'>*</span></label>
                        <select name="id_program_major" id="id_program_major" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programMajorList))
                            {
                                foreach ($programMajorList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4" id="view_minor" style="display: none;">
                    <div class="form-group">
                        <label>Program Minor <span class='error-text'>*</span></label>
                        <select name="id_program_minor" id="id_program_minor" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programMinorList))
                            {
                                foreach ($programMinorList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                
                
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="<?php echo '../../../programmeLandscapeList/' . $id_programme; ?>" class="btn btn-link">Back</a>
            </div>
        </div>

    

        <form id="form_profile" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Course Details</h4>
  
   <!--  <div class="form-container">
        <h4 class="form-group-title">Profile Details</h4>   -->
    
        <div class="m-auto text-center">
           <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
         </div>
        <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Added Courses</a>
            </li>
           <!--  <li role="presentation"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">Major Course </a>
            </li>
            <li role="presentation"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Minor Course</a>
            </li>
            <li role="presentation"><a href="#profile" class="nav-link border rounded text-center"
                    aria-controls="profile" role="tab" data-toggle="tab">Not Compulsary</a>
            </li> -->
        </ul>



        <div class="tab-content offers-tab-content">
            <div role="tabpanel" class="tab-pane active" id="education">
            <div class="col-12 mt-4">
                <br>


            <div class="form-container">
            <h4 class="form-group-title">Course Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <!-- <th>Landscape Name</th> -->
                        <th><?php echo $programmeLandscapeDetails->program_landscape_type; ?></th>
                        <th>Course Name</th>
                        <th>Credit hours</th>
                        <!-- <th>Course Type</th> -->
                        <!-- <th>Pre Requisite</th> -->
                        <th class="text-center">Requisite</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($getCompulsoryCourse)) {
                        $i=1;
                        foreach ($getCompulsoryCourse as $record) {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <!-- <td><?php echo $record->programName ?></td> -->
                            <!-- <td><?php echo $record->semester_code . " - " . $record->semester_name ?></td> -->
                            <td><?php echo $record->id_semester ?></td>
                            <td><?php echo $record->course_code . " - " . $record->course_name ?></td>
                            <td><?php echo $record->credit_hours ?></td>
                            <!-- <td><?php echo $record->courseTypename ?></td> -->
                            <!-- <td><?php echo $record->pre_requisite ?></td> -->
                            <td class="text-center"><?php 

                                foreach ($record->compulsary_requisites as $value)
                                {
                                    if($value->course_code != '')
                                    {
                                        ?>
                                         <a title="<?php echo $value->course_name; ?>">
                                         <?php 
                                        echo $value->course_code;
                                        ?>
                                        <br>
                                        <?php
                                    }
                                }

                             ?></td>
                            <td class="text-center">
                               <a href="<?php echo '../../../addRequisite/'  . $record->id . '/' . $programmeLandscapeDetails->id . '/'. $id_programme . '/' . $id_intake . '/' . $record->id_course . '/Normal' ; ?>" title="Add Courses">Requisite</a>
                                |
                              <?php echo anchor('setup/programmeLandscape/delete_course_program?id='.$record->id, 'Delete', 'id="$record->id"'); ?>
                               
                            </td>
                          </tr>
                      <?php
                      $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
            </div>
             
             </div> <!-- END col-12 -->  
            </div>

            <div role="tabpanel" class="tab-pane" id="proficiency">
                <div class="col-12 mt-4">
                    <br>


                <div class="form-container">
                <h4 class="form-group-title">Major Course Details</h4>  

                     <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th><?php echo $programmeLandscapeDetails->program_landscape_type; ?></th>
                            <th>Program Major</th>
                            <th>Course</th>
                            <th>Credit Hours</th>
                            <th>Course Type</th>
                            <!-- <th>Pre Requisite</th> -->
                            <th class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($getMajorCourse)) {
                            $i=1;
                            foreach ($getMajorCourse as $record) {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <!-- <td><?php echo $record->programName ?></td> -->
                            <td><?php echo $record->id_semester ?></td>
                                <td><?php echo $record->major_code . " - " . $record->major_name ?></td>
                                <td><?php echo $record->course_code . " - " . $record->course_name ?></td>
                                <td><?php echo $record->credit_hours ?></td>
                                <td><?php echo $record->course_type ?></td>
                                <!-- <td><?php echo $record->pre_requisite ?></td> -->
                                <td class="text-center">
                                  <?php echo anchor('setup/programmeLandscape/delete_course_program?id='.$record->id, 'DELETE', 'id="$record->id"'); ?>
                                </td>
                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                </div>
                             
                </div> <!-- END col-12 -->  
            </div>


            <div role="tabpanel" class="tab-pane" id="employment">
                <div class="col-12 mt-4">
                    <br>

            <div class="form-container">
                <h4 class="form-group-title">Minor Course Details</h4>  

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th><?php echo $programmeLandscapeDetails->program_landscape_type; ?></th>
                        <th>Program Minor</th>
                        <th>Course Name</th>
                        <th>Credit Hours</th>
                        <th>Course Type</th>
                        <!-- <th>Pre Requisite</th> -->
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($getMinorCourse)) {
                        $i=1;
                        foreach ($getMinorCourse as $record) {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <!-- <td><?php echo $record->programName ?></td> -->
                            <td><?php echo $record->id_semester ?></td>
                            <td><?php echo $record->minor_code . " - " . $record->minor_name ?></td>
                            <td><?php echo $record->course_code . " - " . $record->course_name ?></td>
                            <td><?php echo $record->credit_hours ?></td>
                            <td><?php echo $record->course_type ?></td>
                            <!-- <td><?php echo $record->pre_requisite ?></td> -->
                            <td class="text-center">
                              <?php echo anchor('setup/programmeLandscape/delete_course_program?id='.$record->id, 'DELETE', 'id="$record->id"'); ?>
                            </td>
                          </tr>
                      <?php
                      $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>
                         
             </div> <!-- END col-12 -->  
            </div>

        
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="col-12 mt-4">
                    <br>




                <div class="form-container">
                <h4 class="form-group-title">Not Compulsary Details</h4>  

                 <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <!-- <th>Landscape Name</th> -->
                        <th><?php echo $programmeLandscapeDetails->program_landscape_type; ?></th>
                        <th>Course Name</th>
                        <th>Credit Hours</th>
                        <th>Course Type</th>
                        <!-- <th>Pre Requisite</th> -->
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($getNotCompulsoryCourse)) {
                        $i=1;
                        foreach ($getNotCompulsoryCourse as $record) {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <!-- <td><?php echo $record->programName ?></td> -->
                            <td><?php echo $record->id_semester ?></td>
                            <td><?php echo $record->course_code . " - " . $record->course_name ?></td>
                            <td><?php echo $record->credit_hours ?></td>
                            <td><?php echo $record->course_type ?></td>
                            <!-- <td><?php echo $record->pre_requisite ?></td> -->
                            <td class="text-center">
                              <?php echo anchor('setup/programmeLandscape/delete_course_program?id='.$record->id, 'DELETE', 'id="$record->id"'); ?>
                            </td>
                          </tr>
                      <?php
                      $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>

       
                 
             </div> <!-- END col-12 -->  
            </div>

          </div>
        </div>

       </div> <!-- END row-->
    </div>
    </form>



   </div> <!-- END row-->
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
    
    function showProgramDetails()
    {
        var type = $("#course_type").val();
        // alert(type);

        if(type == 'Major')
        {
            $("#view_minor").hide();
            $("#view_major").show();
        }
        else if(type == 'Minor')
        {
            $("#view_minor").show();
            $("#view_major").hide();
        }else
        {
            $("#view_minor").hide();
            $("#view_major").hide();
        }

    }

    $(document).ready(function() {
        $("#form_programme_landscape").validate({
            rules: {
                name:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                id_course:
                {
                    required: true
                },
                course_type:
                {
                    required: true
                },
                pre_requisite:
                {
                    required: true
                },
                total_semester:
                {
                    required: true
                },
                total_block:
                {
                    required: true
                },
                total_level:
                {
                    required: true
                },
                min_total_score:
                {
                    required: true
                },
                min_pass_subject:
                {
                    required: true
                },
                id_program_major:
                {
                    required: true
                },
                id_program_minor:
                {
                    required: true
                }
            },
            messages:
            {
                name: {
                    required: "<p class='error-text'>Program Landscape Name Required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                course_type: {
                    required: "<p class='error-text'>Select Course Type</p>",
                },
                pre_requisite: {
                    required: "<p class='error-text'>Select Pre Requisite</p>",
                },
                total_semester: {
                    required: "<p class='error-text'>Enter Total Semester</p>",
                },
                total_block: {
                    required: "<p class='error-text'>Enter Total Block</p>",
                },
                total_level: {
                    required: "<p class='error-text'>Enter Total Level</p>",
                },
                min_total_score: {
                    required: "<p class='error-text'>Enter Min Total Score</p>",
                },
                min_pass_subject: {
                    required: "<p class='error-text'>Enter Minimum Pass Subject</p>",
                },
                id_program_major: {
                    required: "<p class='error-text'>Select Program Major</p>",
                },
                id_program_minor: {
                    required: "<p class='error-text'>Select Program Minor</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
