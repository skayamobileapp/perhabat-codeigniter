<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <!-- <div class="page-title clearfix">
            <h3>Welcome : Module SETUP</h3>
        </div> -->
    <?php

    if($id_role == 1)
    {
        ?>


        <div class="row">

            <div class="col-sm-3">
                <div class="stats-col">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_approval_icon.svg" />Active Products
                    </div>
                    <div class="count">
                        <?php echo $programme_active_count; ?>
                    </div>
                    <div class="text-center">
                        <a href="/prdtm/programme/list" title="Redirects To Corporate Approvals">More Info</a>
                    </div>
                </div>
            </div>

            <!-- <div class="col-sm-3">
                <div class="stats-col active-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/active_students_icon.svg"/>Students
                    </div>
                    <div class="count">
                        <?php echo $student_count; ?>
                    </div>
                    <div class="text-center">
                        <a href="/records/studentRecord/list" title="Redirects To Records">More Info</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="stats-col pending-acceptance">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_acceptance_icon.svg"/>Active Company
                    </div>
                    <div class="count">
                        <?php echo $company_active_count; ?>
                    </div>
                    <div class="text-center">
                        <a href="/corporate/company/list" title="Redirects To Corporate List">More Info</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="stats-col inactive-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/inactive_students_icon.svg"/>Employees
                    </div>
                    <div class="count">
                        <?php echo $employee_count; ?>
                    </div>
                    <div class="text-center">
                        <a href="/records/studentRecord/list" title="Redirects To Records">More Info</a>
                    </div>
                </div>
            </div> -->

        </div>

        <hr/>



    


        <div class="row">            
            <div class="col-sm-3 col-lg-2">
                <a href="/setup/welcome" class="dashboard-menu"><span class="icon"></span>System Setup</a>
            </div> 
            <!-- <div class="col-sm-3 col-lg-2">
                <a href="/communication/welcome" class="dashboard-menu communication-management"><span class="icon"></span>Communication Management</a>
            </div> -->
        </div>



        <?php
    }
    else
    {
        ?>

        <div class="welcome-container">
            <img src="<?php echo BASE_PATH; ?>assets/img/system_setup_icon.svg" alt="Partner Management Module">
            <h3>Welcome to <br/><strong>Setup Module</strong></h3>
            <p>Text........................</p>
        </div>


        <?php
    }
    ?>

        <footer class="footer-wrapper">
            <p>&copy; 2020 All rights, reserved</p>
        </footer>

    </div>
</div>