            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li ><a href="/livechat/php/app.php?admin" target="_blank">Chat</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                   
                    </div>
                    <div class="sidebar-nav">                            
                        <h4><a role="button" data-toggle="collapse" class="collapsed" href="#generalSetup">General Setup <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="collapse" id="generalSetup">
                            <li><a href="/setup/role/list">Roles</a></li>
                            <li><a href="/setup/user/list">User</a></li>
                            <li><a href="/setup/permission/list">Permissions</a></li>
                            <li><a href="/setup/salutation/list">Salutation</a></li>
                            <!-- <li><a href="#">Change Password</a></li> -->
                            <li><a href="/setup/educationLevel/list">Education Level</a></li>
                            <li><a href="/setup/award/list">Award</a></li>
                            <!-- <li ><a href="/setup/country/list">Countries</a></li>
                            <li><a href="/setup/state/list">States</a></li> -->
                        </ul>
                        <h4><a role="button" data-toggle="collapse" class="collapsed" href="#demographicSetup">Demographic Setup<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="collapse" id="demographicSetup">
                            <li><a href="/setup/race/list">Race</a></li>
                            <li><a href="/setup/religion/list">Religion</a></li>
                            <li><a href="/setup/nationality/list">Nationality</a></li>
                        </ul>
                        <h4><a role="button" data-toggle="collapse" class="collapsed" href="#organisationSetup">Organisation Setup<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="collapse" id="organisationSetup">
                            <li><a href="/setup/organisation/edit">University Setup</a></li>
                            <li><a href="/setup/facultyProgram/list">Faculty Program</a></li>
                            <li><a href="/setup/department/list">Department</a></li>
                        </ul>

                        <h4><a role="button" data-toggle="collapse" class="collapsed" href="#staffSetup">Staff<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="collapse" id="staffSetup">
                            <li><a href="/setup/staff/list">Staff master</a></li>
                        </ul>
                        <h4><a role="button" data-toggle="collapse" class="collapsed" href="#documentsSetup">Documents<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="collapse" id="documentsSetup">
                            <li><a href="/setup/documents/list">Documents Required</a></li>
                            <!-- <li><a href="/setup/documentsProgram/list">Documents For Program</a></li> -->
                        </ul>
                        <h4><a role="button" data-toggle="collapse" class="collapsed" href="#operationSetup">Operation Setup<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="collapse" id="operationSetup">
                            <li><a href="/setup/scheme/list">Scheme</a></li>
                            <li><a href="/setup/academicYear/list">Academic Year</a></li>
                            <li><a href="/setup/intake/list">Intake</a></li>
                            <li><a href="/setup/semester/list">Semester</a></li>
                            <li><a href="/setup/activityDetails/list">Activity</a></li>
                            <li><a href="/setup/courseOffered/list">Course Offered</a></li>
                            <!-- <li><a href="/setup/staffHasCourse/list">Staff Has Course</a></li> -->
                        </ul>


                        
                        <!-- <h4>University Setup</h4>
                        <ul>
                            <li><a href="/setup/partnerCategory/list">Partner Category</a></li>
                            <li><a href="/setup/partnerUniversity/list">Partner University</a></li>

                        </ul>
                        <h4>Course Setup</h4>
                        <ul>
                            <li><a href="/setup/courseDescription/list">Course Major / Minor</a></li>
                            <li><a href="/setup/courseType/list">Course Type</a></li>
                            <li><a href="/setup/course/list">Course Main</a></li>
                            <li><a href="/setup/courseMaster/list">Course Requisite</a></li>
                            <li><a href="/setup/equivalentCourse/list">Course Equivalent</a></li>
                        </ul>

                        

                        <h4>Program Setup</h4>
                        <ul>
                            <li><a href="/setup/landscapeCourseType/list">Landscape Course Type</a></li>
                            <li><a href="/setup/programType/list">Program Type</a></li>
                            <li><a href="/setup/programme/list">Program Main</a></li>
                            <li class="active"><a href="/setup/programmeLandscape/list">Program Landscape</a></li>
                            <li><a href="/setup/individualEntryRequirement/list">Program Entry Requirement</a></li>
                        </ul>
                        
                        <h4>Registration</h4>
                        <ul>
                            <li><a href="/setup/courseWithdraw/list">Course Withdraw</a></li>
                            <li><a href="/setup/lateRegistration/list">Late Registration</a></li>
                        </ul>

                        <h4>Report</h4>
                        <ul>
                            <li><a href="/setup/user/chart1">Students by Intake</a></li>
                            <li><a href="/setup/user/chart2">Applicant V/S Lead V/S Student</a></li>
                            <li><a href="/setup/user/chart3">Course Wise data</a></li>
                        </ul> -->
                   
                </div>
            </div>