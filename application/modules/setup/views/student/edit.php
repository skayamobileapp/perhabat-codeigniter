<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Student</h3>
        </div>
        <form id="form_academic_year" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>First Name <span class='error-text'>*</span></label>
                        <br>
                        <input type="text" class="form-control"  id="first_name" name="first_name" autocomplete="off" value="<?php echo $student->first_name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Last Name <span class='error-text'>*</span></label>
                        <br>
                        <input type="text" class="form-control"  id="last_name" name="last_name" autocomplete="off" value="<?php echo $student->last_name;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" autocomplete="off" value="<?php echo $student->contact_number;?>">
                    </div>
                </div>


            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" autocomplete="off" value="<?php echo $student->email;?>">
                    </div>
                </div>





                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="gender" value="Male" <?php if($student->gender=='Male') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Male
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="gender" value="Female" <?php if($student->gender=='Female') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> Female
                            </label>                              
                        </div>                         
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>DOB <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="dob" name="dob" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($student->dob));?>">
                    </div>
                </div>


            </div>


            <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Father Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="father_name" name="father_name" autocomplete="off" value="<?php echo $student->father_name;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="bank_name" name="bank_name" autocomplete="off" value="<?php echo $student->bank_name;?>">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="account_number" name="account_number" autocomplete="off" value="<?php echo $student->account_number;?>">
                    </div>
                </div>


            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Highest Education *</label>
                        <select name="highest_education" id="highest_education" class="form-control">
                            <option value="">Select</option>
                             <option value="School Level"<?php if($student->highest_education == "School Level"){ echo "selected=selected";} ?>>School Level</option>
                             <option value="Intermediate"<?php if($student->highest_education == "Intermediate"){ echo "selected=selected";} ?>>Intermediate</option>
                             <option value="Batcholar Degree"<?php if($student->highest_education == "Batcholar Degree"){ echo "selected=selected";} ?>>Batcholar Degree</option>
                             <option value="Master Degree"<?php if($student->highest_education == "Master Degree"){ echo "selected=selected";} ?>>Master Degree</option>
                             <option value="Other"<?php if($student->highest_education == "Other"){ echo "selected=selected";} ?>>Other</option>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year Of Passing <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="year_of_passing" name="year_of_passing" autocomplete="off" max="<?php echo date('Y') ?>"  min="<?php echo date('Y') - 50; ?>" value="<?php echo $student->year_of_passing;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="Pending" <?php if($student->status=='Pending') {
                                 echo "checked=checked";
                              };?> disabled><span class="check-radio"></span> Pending
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="Approved" <?php if($student->status=='Approved') {
                                 echo "checked=checked";
                              };?> disabled><span class="check-radio"></span> Approved
                            </label>                              
                        </div>                         
                </div>


            </div>

        </div>



        <div class="form-container">
            <h4 class="form-group-title">Contact Details</h4>

                <div class="row">
  

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $student->address;?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address 2 </label>
                            <input type="text" class="form-control" id="address_two" name="address_two" value="<?php echo $student->address_two;?>">
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Zipcode <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="zipcode" name="zipcode" maxlength="6" value="<?php echo $student->zipcode; ?>">
                        </div>
                    </div> 

                     

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Country <span class='error-text'>*</span></label>
                            <select name="country_id" id="country_id" class="form-control" onchange="getStateByCountry(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id; ?>"<?php if($record->id == $student->country_id)
                                {echo "selected=selected";} ?>><?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select State <span class='error-text'>*</span></label>
                            <!-- <select class="form-control" id='state_id' name='state_id'>
                                <option value=''></option>
                            </select> -->
                            <span id="state_view"></span>
                        </div>
                    </div>
            
                </div>

        </div>


        <div class="form-container">
            <h4 class="form-group-title">Training Details</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Training Center <span class='error-text'>*</span></label>
                            <select name="training_center_id" id="training_center_id" class="form-control" onchange="getJobRolesByTrainingCenterId(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($trainingCenterList))
                                {
                                    foreach ($trainingCenterList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>"<?php if($record->id == $student->training_center_id){ echo "selected=selected";} ?>><?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Job Roles <span class='error-text'>*</span></label>
                            <!-- <select class="form-control" id='state_id' name='state_id'>
                                <option value=''></option>
                            </select> -->
                            <span id="job_role_view"></span>
                        </div>
                    </div>
    
                </div>


        </div>










        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    $( function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });


    var country_id, state_id, training_center_id, job_role_id;

    $(document).ready(function()
    {
        <?php 
        if(!empty($student))  
        {
        ?>
            var id = "<?php echo $student->id ?>";
            country_id = "<?php echo $student->country_id; ?>";
            state_id = "<?php echo $student->state_id; ?>";
            job_role_id = "<?php echo $student->job_role_id; ?>";
            training_center_id = "<?php echo $student->training_center_id; ?>";

            // console.log(training_center_id);
            $('#country_id').val(country_id).trigger('change');
            $('#training_center_id').val(training_center_id).trigger('training_center_id');
    
        <?php
        }
        ?>

        $("#form_academic_year").validate({
            rules: {
                name: {
                    required: true
                },
                email:
                {
                    required: true
                },
                contact_person:
                {
                    required: true
                },
                serial_number:
                {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                contact_person:
                {
                    required: "<p class='error-text'>Contact person Required</p>",
                },
                serial_number:
                {
                    required: "<p class='error-text'>Serial Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    function getStateByCountry(id)
    {
        country_id = $("#country_id").val();
        // console.log(country_id);
        $.get("/setup/staff/getStateByCountry/"+country_id, function(data, status)
        {
            $("#state_view").html(data);
            // console.log(state_id);
            if(state_id > 0)
            {
                $('#state_id').val(state_id).trigger('state_id');
            }
        });
    }


    function getJobRolesByTrainingCenterId(id)
    {
        console.log(id);
        training_center_id = $("#training_center_id").val();
        $.get("/setup/student/getJobRolesByTrainingCenterId/"+training_center_id, function(data, status)
        {
            console.log(data);
            $("#job_role_view").html(data);
            if(job_role_id > 0)
            {
                $('#job_role_id').val(job_role_id).trigger('job_role_id');
            }
        });
    }

</script>
