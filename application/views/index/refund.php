   <!--PAGE HEADER STARTS HERE-->

    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4">Refund Policy</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->

    <!--LIST PAGE  STARTS HERE-->

    <div class="filter-wrapper py-2">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            If you decide not to complete a course and have made full payment, you may request for a refund of your payment according to the following guidelines:
          </div>
        </div>
         <div class="row">
          <div class="col-sm-12">
            <ul>
              <li>Refund of 75% of the full payment made upfront if your request for withdrawal is within a quarter (25%) of the duration of the course
               <ul>
                <li>Eg : 
                For a 4 week course, if you request to withdraw within Week 1 of the commencement of the course, you may be refunded 75% of the full payment for the course. </li>
               </ul></li> <br/>
               <li>Refund of 25% of the full payment made upfront if you request to withdraw within half-way (50%) of the duration of the course.
               <ul>
                <li>Eg : 
                For a 4 week course, if you request to withdraw within Week 2 of the commencement of the course, you may be refunded 25% of the full payment for the course.</li>
               </ul></li><br/>
                <li>No refund of full payment for the course, if you withdraw more than half-way the duration of the course.     
               <ul>
                <li>Eg : 
                For a 4 week course, if you request to withdraw after Week 2 of the commencement of the course, you may not be refunded of the full payment for the course.</li>
               </ul></li>

            </ul>
          </div>
        </div>
        
      </div>
    </div>

      <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link" href="/index/terms">Terms of Use</a>
              <a class="nav-link" href="/index/refund">Refund Policy</a>
            </nav>
          </div>
        </div>
      </div>
    </div>