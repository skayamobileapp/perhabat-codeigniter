   <!--PAGE HEADER STARTS HERE-->

    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <?php if($coursetype=='1') {?>
              <h1 class="mb-0 text-white display-4">Short Courses</h1>
                            <p class="text-white">Short Courses provide small segments of learning that focus on practical application of knowledge and skills in a short period of time. They are generally affordable, easy to take, and provide a way for you to acquire new skills, upgrade your current skills or acquire multiple skills. The duration of Short Courses may range from a few days to several weeks or months depending on the discipline, the workload and the objectives of the course. Short Courses seek to advance your skill sets within a given career field that interests you. Short Courses may be taken as stand-alone</p>

              <?php } else if($coursetype=='2') {?>
              <h1 class="mb-0 text-white display-4">Microcredential</h1>
                                          <p class="text-white">Microcredentials are short segments of learning which range from 1, 2 or more credits that is either from a component of an accredited programme or a stand-alone course. The duration of a Microcredential may range from a few weeks to a semester. When completed and meeting all assessment requirements, you will be awarded with a Certificate of Achievement which you could use to apply for credit transfer to a related accredited programme offered by an institution which accepts such qualification. These short learning chunks focus on specific professional skills to ensure that </p>


              <?php } else if($coursetype=='3') {?>
              <h1 class="mb-0 text-white display-4">Professional Courses</h1>
              <p class="text-white">Professional Courses are specifically designed to meet industry requirements and fulfill certain requirements of professional bodies and industry. Professional Courses are becoming a popular alternative to academics. Entry requirements vary depending on the qualification and many professional qualifications are mandatory for entry into a specific industry or profession. Professional Courses may be taken as stand-alone courses and accumulated leading to an industry-recognised professional qualification. These qualifications provide a well-defined career in specific industry </p>

              <?php } else {?>
              <h1 class="mb-0 text-white display-4">List Page</h1>

              <?php } ?> 
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->

    <!--LIST PAGE  STARTS HERE-->

    <div class="filter-wrapper py-2">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-auto filter-text">Filter By:</div>
          <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="categoryFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Category <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                    <?php for($i=0;$i<count($categoryList);$i++) { ?> 

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $categoryList[$i]->id;?>"                    
                    id="courseType<?php echo $categoryList[$i]->id;?>"
                    name="courseType[]"

                  />
                  <label class="custom-control-label" for="courseType<?php echo $categoryList[$i]->id;?>"
                    ><?php echo $categoryList[$i]->name;?></label
                  >
                </div>
                   <?php }?> 
              
                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>
        
          <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="studyLevelFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Skill Level <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                <?php for($i=0;$i<count($studyLevelList);$i++) { ?> 

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $studyLevelList[$i]->id;?>"                    
                    id="studyType<?php echo $studyLevelList[$i]->id;?>"
                    name="studyType[]"

                  />
                  <label class="custom-control-label" for="studyType<?php echo $studyLevelList[$i]->id;?>"
                    ><?php echo $studyLevelList[$i]->name;?></label
                  >
                </div>
                   <?php }?> 


                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>


           <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="productTypeFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Course Type <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                  <?php for($i=0;$i<count($productTypeList);$i++) { ?> 
                <div class="custom-control custom-checkbox mb-1">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $productTypeList[$i]->id;?>"                    
                    id="productType<?php echo $productTypeList[$i]->id;?>"
                    name="productType[]"
  <?php if($productTypeList[$i]->id==$catid) { ?>
                      checked=""

                    <?php } ?> 


                  />
                  <label class="custom-control-label" for="productType<?php echo $productTypeList[$i]->id;?>"
                    ><?php echo $productTypeList[$i]->name;?></label
                  >
                </div>

              <?php } ?> 
                
                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>

          <div class="col-md-auto">
            <div class="btn-group filter-dropdown">
              <button
                type="button"
                class="btn btn-primary"
                onclick="getProducts()"
              >
                Apply Now
              </button>
            </div>
          </div>


        </div>
      </div>
    </div>
  <div class="py-6 course-lists-container">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="tab-content">
              <div class="tab-pane fade pb-4 active show" id="tabPaneGrid" role="tabpanel" aria-labelledby="tabPaneGrid">
                <div class="row pb-lg-3 pt-lg-3 course-card" id="appendTableDiv">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="pt-5" id="loadingicon" style="display: none;">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 offset-lg-1 mb-5">
            <div class="card mb-3">
              <div class="card-body text-center">
                <span class="loader-icon">
                  <i class="fe fe-loader"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--LIST ENDS  STARTS HERE-->

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
                          <a class="nav-link" href="/index/terms">Terms of Use</a>
              <a class="nav-link" href="/index/refund">Refund Policy</a>

            </nav>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->




    <script
      src="//code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="/website/js/main.js"></script>

   <script>

    $( document ).ready(function() {
      var cn = '<?php echo $type;?>';
      if(cn=='course') {
        var productIds = '0';
        var courseIds = '0,1';
         $.post('/course/getprogramme', {productyType:productIds, courseType:courseIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
      }

      else if(cn=='program') {
        var productIds = '0';
        var courseIds = '0,2';
         $.post('/course/getprogramme', {productyType:productIds, courseType:courseIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
      }

       else {

         $.post('/course/getprogramme', {productname:cn}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
      }
  });

   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

     function getProducts()
    {

      $("#loadingicon").show();
      var productIds = '0';
        $.each($("input[name='productType[]']:checked"), function() {
          productIds = productIds+","+$(this).val();
        });


         var courseIds = '0';
        $.each($("input[name='courseType[]']:checked"), function() {
          courseIds = courseIds+","+$(this).val();
        });


         var studyIds = '0';
        $.each($("input[name='studyType[]']:checked"), function() {
          studyIds = studyIds+","+$(this).val();
        });



       $.post('/course/getprogramme', {productyType:productIds, courseType:courseIds,studyType:studyIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
    }

  </script>
  


  </body>
</html>
