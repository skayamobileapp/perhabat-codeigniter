    <div class="pt-lg-8 pb-lg-16 pt-8 pb-12 course-wrapper">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-7 col-lg-7 col-md-12">
            <div>
              <h1 class="text-white font-weight-semi-bold">
                                <?php echo $programmeList->name;?>

              </h1>
              <div class="text-white mb-6">
              </div>
              <div class="d-flex align-items-center">
                <a
                  href="#!"
                  class="text-white text-decoration-none"
                  data-toggle="tooltip"
                  data-placement="top"
                  title=""
                  data-original-title="Add to
                           Bookmarks"
                >
                  <i class="fe fe-clock text-white-50 mr-2"></i>
                  <?php echo $programmeList->max_duration.' '.$programmeList->duration_type;?>
                </a>
                <span class="text-white ml-3"
                  ><i class="fe fe-clock text-white-50"></i> <?php echo $programmeList->student_learning_hours;?> - Learning Hours
                </span>

                 <span class="text-white ml-3"
                  ><i class="fe fe-book text-white-50"></i> <?php echo $programmeList->studyLevel;?>
                </span>
                
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="pb-10">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-12 col-12 mt-n8 mb-4 mb-lg-0">
            <!-- Card -->
            <div class="card rounded-lg">
              <!-- Card header -->
              <div class="card-header border-bottom-0 p-0">
                <div>
                  <!-- Nav -->
                  <ul class="nav nav-lb-tab" id="tab" role="tablist">
                   
                    <li class="nav-item">
                      <a
                        class="nav-link active"
                        id="description-tab"
                        data-toggle="pill"
                        href="#description"
                        role="tab"
                        aria-controls="description"
                        aria-selected="false"
                        >Overview</a
                      >
                    </li>

                    
                    <?php if(count($syllabusList)>0) { ?> 

                    <li class="nav-item">
                      <a
                        class="nav-link"
                        id="objective-tab"
                        data-toggle="pill"
                        href="#objective"
                        role="tab"
                        aria-controls="objective"
                        aria-selected="false"
                        >Objective</a
                      >
                    </li>
                  <?php } ?> 

                    <?php if(count($topicList)>0) { ?> 

                     <li class="nav-item">
                      <a
                        class="nav-link"
                        id="table-tab"
                        data-toggle="pill"
                        href="#table"
                        role="tab"
                        aria-controls="table"
                        aria-selected="false"
                        >Topic</a
                      >
                    </li>
                  <?php } ?> 

                    <?php if(count($assessmentList)>0) { ?> 

                    <li class="nav-item">
                      <a
                        class="nav-link"
                        id="review-tab"
                        data-toggle="pill"
                        href="#review"
                        role="tab"
                        aria-controls="review"
                        aria-selected="false"
                        >Assessment</a
                      >
                    </li>
                  <?php }?> 


                    <?php if(count($programmeAwardList)>0) { ?> 
                    <li class="nav-item">
                      <a
                        class="nav-link"
                        id="faq-tab"
                        data-toggle="pill"
                        href="#faq"
                        role="tab"
                        aria-controls="faq"
                        aria-selected="false"
                        >Award</a
                      >
                    </li>
                  <?php } ?> 

                    <?php if(count($referencesList)>0) { ?> 
                    <li class="nav-item">
                      <a
                        class="nav-link"
                        id="reference-tab"
                        data-toggle="pill"
                        href="#reference"
                        role="tab"
                        aria-controls="reference"
                        aria-selected="false"
                        >References</a
                      >
                    </li>
                  <?php } ?> 

                  </ul>
                </div>
              </div>
              <!-- Card Body -->
              <div class="card-body">
                <div class="tab-content" id="tabContent">
                  <div
                    class="tab-pane fade"
                    id="table"
                    role="tabpanel"
                    aria-labelledby="table-tab"
                  >
                    <!-- Card -->
                    <div
                      class="accordion course-accordion"
                      id="courseAccordion"
                    >
                      <div>
                        <!-- List group -->
                        <ul class="list-group list-group-flush">

                          <?php for($i=0;$i<count($topicList);$i++) {
                            $j = $i+1; ?>
                          <li class="list-group-item px-0 pt-2">
                            <!-- Toggle -->
                            <a
                              class="h4 mb-0 d-flex align-items-center text-inherit text-decoration-none"
                              data-toggle="collapse"
                              href="#courseTwo<?php echo $topicList[$i]->id;?>"
                              aria-expanded="false"
                              aria-controls="courseTwo<?php echo $topicList[$i]->id;?>"
                            >
                              <div class="mr-auto">
                                <?php echo $j." - ".$topicList[$i]->topic;?>
                              </div>
                              <!-- Chevron -->
                              <span class="chevron-arrow ml-4">
                                <i class="fe fe-chevron-down font-size-md"></i>
                              </span>
                            </a>
                            <!-- Row -->
                            <!-- Collapse -->
                            <div
                              class="collapse"
                              id="courseTwo<?php echo $topicList[$i]->id;?>"
                              data-parent="#courseAccordion"
                            >
                            
                                <?php  

                $this->load->model('register_model');

              $namesArray = $this->register_model->getNames($topicList[$i]->id_programme_has_syllabus);
              
                              for($l=0;$l<count($namesArray);$l++) { ?>

                                 <div class="pt-3 pb-2">
                                  <div class="text-truncate">
                                    <i
                                      class="fe fe-tick font-size-md mr-2"
                                    ></i>
                                    <span><?php echo $namesArray[$l]->learning_objective;?></span>
                                  </div>
                                                             </div>

                              <?php } ?> 
                                


                              <div class="pt-3 pb-2">
                               
                               
                                  <div class="text-truncate">
                                  <?php echo $topicList[$i]->message;?>
                                    
                                  </div>
                               
                                
                              </div>


                            </div>
                          </li>
                          <?php } ?> 
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div
                    class="tab-pane fade  show active"
                    id="description"
                    role="tabpanel"
                    aria-labelledby="description-tab"
                  >
                    <!-- Description -->
                    <div class="mb-4">
                      <?php echo $overview[0]->overview;?>
                    </div>
                  </div>

                  <div
                    class="tab-pane fade"
                    id="objective"
                    role="tabpanel"
                    aria-labelledby="objective-tab"
                  >
                    <!-- Description -->
                     <div class="mb-4">
                        <table class="table">
                                                 <tbody>
                          <?php for($m=0;$m<count($syllabusList);$m++) { ?> 
                           <tr>
                            <td><?php echo ucfirst($syllabusList[$m]->learning_objective);?></td>
                           
                           </tr>
                         <?php } ?> 
                       </tbody>
                        </table>
                      </div>
                  </div>


                  <div
                    class="tab-pane fade"
                    id="review"
                    role="tabpanel"
                    aria-labelledby="review-tab"
                  >
                    <!-- Reviews -->

                     <div class="mb-4">
                        <table class="table">
                        <thead class="thead-light">
                           <tr>
                            <th>Component</th>
                            <th>Total Marks</th>
                            <th>Passing Marks</th>
                           </tr>
                         </thead>
                         <tbody>
                          <?php for($m=0;$m<count($assessmentList);$m++) { ?> 
                           <tr>
                            <td><?php echo $assessmentList[$m]->component;?></td>
                            <td><?php echo $assessmentList[$m]->total_marks;?></td>
                            <td><?php echo $assessmentList[$m]->passing_mark;?></td>
                           </tr>
                         <?php } ?> 
                       </tbody>
                        </table>
                      </div>

                    
                   
                  </div>

                  <!-- Tab pane -->
                  <div
                    class="tab-pane fade"
                    id="faq"
                    role="tabpanel"
                    aria-labelledby="faq-tab"
                  >
                    <!-- FAQ -->
                    <div class="mb-4">
                        <table class="table">
                        <thead class="thead-light">
                           <tr>
                            <th>Award Name</th>
                           </tr>
                         </thead>
                         <tbody>
                          <?php for($m=0;$m<count($programmeAwardList);$m++) { ?> 
                           <tr>
                            <td><?php echo $programmeAwardList[$m]->name;?></td>
                           </tr>
                         <?php } ?> 
                       </tbody>
                        </table>
                      </div>
                  </div>


                   <div
                    class="tab-pane fade"
                    id="reference"
                    role="tabpanel"
                    aria-labelledby="reference-tab"
                  >
                    <!-- Description -->
                      <div class="mb-4">
                      <?php echo $referencesList[0]->message;?>
                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-12 mt-lg-n22">
            <!-- Card -->
            <div class="card mb-3 mb-4">
              <div class="p-1">
                <div
                  class="d-flex justify-content-center position-relative rounded py-10 border-white border rounded-lg bg-cover"
                  style="background-image: url(/assets/images/<?php echo $programmeList->image;?>)"
                >
                 
                </div>
              </div>
             <!--  <h4 class="text-center mb-0 mt-3">RM  <?php echo $programmeList->amount;?> </h4>
              Card body 
              <div class="card-body">
                <a href="javascript:buynow(<?php echo $programmeList->id;?>,<?php echo $programmeList->amount;?>)" class="btn btn-primary btn-block"> Buy Now</a>
                <a href="javascript:buynow(<?php echo $programmeList->id;?>,<?php echo $programmeList->amount;?>)" class="btn btn-outline-primary btn-block"
                  >Add to Cart</a
                >
              </div> -->
            </div>

            <?php
                            $this->load->model('course_model');

                      $originalFee = $this->course_model->getRegistrationFee($programmeList->id);
                      $registrationFee = 0;
                      $idFeestructure = 0;
                      for($i=0;$i<count($originalFee);$i++) {
                            $registrationFee = $registrationFee+$originalFee[$i]->amount;
                            $idFeestructure = $idFeestructure.'@'.$originalFee[$i]->id;
                      }


                        $certificateFeearray = $this->course_model->getCertificateFee($programmeList->id);
                      $certificateFee = 0;
                      $idFeestructurecert = 0;
                      for($i=0;$i<count($originalFee);$i++) {
                            $certificateFee = $certificateFee+$certificateFeearray[$i]->amount;
                            $idFeestructurecert = $idFeestructurecert.'@'.$originalFee[$i]->id;

                      }


                      ?>

            <div class="card mb-4">
              <div class="card-body">
                <div class="text-center">
                  <h4>Join Now</h4>

                  <?php if($registrationFee>0) { ?>
                  <h3 class="pt-2">RM <?php echo number_format((float)$registrationFee, 2, '.', '');
;?></h3>
 <?php } else { ?> 

    <h3 class="pt-2">FREE</h3>

  <?php } ?> 
                </div>
                 <a href="javascript:buynow(<?php echo $programmeList->id;?>,'<?php echo $idFeestructure;?>',<?php echo $registrationFee;?>)" class="btn btn-primary btn-block">Register Now</a>
              </div>
            </div>

            <?php if($certificateFee>0) {?>
            <div class="card mb-4">
              <div class="card-body">
                <div class="text-center">
                  <h4>Upgrade</h4>
                  <h3 class="pt-2"><?php echo $certificateFee;?></h3>
                </div>
                <a href="javascript:buynow(<?php echo $programmeList->id;?>,<?php echo $idFeestructurecert;?>,<?php echo $certificateFee;?>)" class="btn btn-primary btn-block">Buy Now</a>
              </div>
            </div>
          <?php } ?> 
            <!-- Card -->
            <div class="card mb-4">
              <div>
                <!-- Card header -->
                <div class="card-header">
                  <h4 class="mb-0 h5">What’s included</h4>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item bg-transparent">
                                                   <?php echo $programmeList->short_description;?>

                  </li>
                  <?php if(count($topicList)>0) { ?> 
                  <li class="list-group-item bg-transparent">
                    <i class="fe fe-calendar align-middle mr-2 text-info"></i><?php echo count($topicList);?> 
                    Topics
                  </li>
                <?php } ?> 
                 
                </ul>
              </div>
            </div>
            <!-- Card -->
            <div class="card">
              <!-- Card body -->
              <div class="card-body instructor-details">
                <div class="d-flex align-items-center">
                  <div class="position-relative">

                    <img
                    src="/website/staff/<?php echo $staffDetails->image;?>"
                      alt=""
                      class="rounded-circle avatar-xl"
                    />
                  </div>
                  <div class="ml-4">
                    <h4 class="mb-0"><?php echo $staffDetails->name;?></h4>
                    <p class="mb-1 font-size-xs">
                      <?php echo $staffDetails->degree_details;?>
                    </p>
                    <span class="font-size-xs"
                      ><span class="text-warning">5.0 </span
                      ><img src="/website/img/star_icon.svg" alt="star" />Instructor
                      Rating</span
                    >
                  </div>
                </div>
                <div
                  class="border-top row mt-3 border-bottom mb-3 no-gutters stats"
                >
                  <div class="col">
                    <div class="pr-1 pl-2 py-3">
                      <h5 class="mb-0">11,604</h5>
                      <span>Students</span>
                    </div>
                  </div>
                  <div class="col border-left">
                    <div class="pr-1 pl-3 py-3">
                      <h5 class="mb-0">32</h5>
                      <span>Courses</span>
                    </div>
                  </div>
                  <div class="col border-left">
                    <div class="pr-1 pl-3 py-3">
                      <h5 class="mb-0">12,230</h5>
                      <span>Reviews</span>
                    </div>
                  </div>
                </div>
                <p>
                  Professor John Arul Phillips previously served the Faculty of Education, University of Malaya (UM) and Open University Malaysia (OUM) engaged in online distance teacher education. Currently, Professor Phillips is Dean, School and Cognitive Science, Asia e University (AeU) involved in preparing educators at the bachelors, masters and doctoral levels through online distance learning.
                </p>
                <!-- <a href="#" class="btn btn-outline-secondary btn-sm"
                  >View Details</a
                > -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- COURSE DETAILS STARTS HERE-->

   

   
  <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
                          <a class="nav-link" href="/index/terms">Terms of Use</a>
              <a class="nav-link" href="/index/refund">Refund Policy</a>

            </nav>
          </div>
        </div>
      </div>
    </div>
     <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>

    <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>

 <script>

   function buynow(id,feestructure,amount)
    {
      $.noConflict();

        // jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
        //      console.log(data);
        //  });


       jQuery.post('/coursedetails/tempbuynow', {id_programme:id, id_feestructure:feestructure,amount:amount}, function(response){ 
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
        });

    }


   

  </script>



