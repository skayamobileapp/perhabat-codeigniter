<body>
    <div class="container d-flex flex-column">
        <div class="row align-items-center justify-content-center no-gutters min-vh-100">
            <div class="col-lg-5 col-md-8 py-8 py-xl-0">
                <form action="" method="post" class="px-lg-4">
                        <div class="card shadow">
                            <div class="card-body p-6">
                            <div class="mb-4">
                                <h2 class="mb-1 font-weight-bold">Partner University Registration</h2>
                                <h2>
                                  <?php
                                  $this->load->helper('form');
                                  $error = $this->session->flashdata('error');
                                  if ($error) {
                                  ?>
                                    <div class="alert alert-danger alert-dismissable">
                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                      <?php echo $error; ?>
                                    </div>
                                  <?php }
                                  $success = $this->session->flashdata('success');
                                  $entered_url = $this->session->flashdata('entered_url');
                                  // print_r($success);exit();
                            
                                  if ($success)
                                  {
                                  ?>
                                      <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <?php echo $success; ?>
                                      </div>
                                  <?php
                                  }
                                  ?>
                                </h2>                                
                            </div>    
                            <div class="form-group">
                                <label>Code <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="code" name="code" onblur="getPartnerUniversityCodeDuplication()" required>
                            </div>
                
                
                            <div class="form-group">
                                <label>Name <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="name" name="name" onblur="getPartnerUniversityNameDuplication()" required>
                            </div>
                
                
                            <div class="form-group">
                                <label>Short Name <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="short_name" name="short_name" required>
                            </div>
                
                
                            <div class="form-group">
                                <label>Login Id <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="login_id" name="login_id" onblur="getPartnerUniversityLoginIdDuplication()" required>
                            </div>
                            
                
                            <div class="form-group">
                                <label>Password <span class='error-text'>*</span></label>
                                <input type="password" class="form-control" id="password" name="password" required>
                            </div>
                
                
                            <div class="form-group">
                                <label>Contact Email <span class='error-text'>*</span></label>
                                <input type="email" class="form-control" id="email" name="email" onblur="getPartnerUniversityEmailIdDuplication()" required>
                            </div>

                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                
                          <div class="login-links">
                            <hr />
                            <p>Have an account? <a href="/partnerUniversityLogin">Login</a></p>
                          </div>   
                          </div>
                        </div>
                            
                            
                
                
                    </form>                
            </div>
        </div>
    </div>


  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
</body>
</html>
<script>

  function getPartnerUniversityCodeDuplication()
  {
      var code = $("#code").val()

      if(code != '')
      {

        var tempPR = {};
        tempPR['code'] = code;
        tempPR['name'] = '';
        tempPR['login_id'] = '';
        tempPR['email'] = '';
        tempPR['id_partner'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/partnerUniversityLogin/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Code Not Allowed, Partner Already Registered With The Given Code : '+ code );
                  $("#code").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityNameDuplication()
  {
      var name = $("#name").val()

      if(name != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = name;
        tempPR['login_id'] = '';
        tempPR['email'] = '';
        tempPR['id_partner'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/partnerUniversityLogin/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Name Not Allowed, Partner Already Registered With The Given Name : '+ name );
                  $("#name").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityLoginIdDuplication()
  {
      var login_id = $("#login_id").val()

      if(login_id != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = '';
        tempPR['login_id'] = login_id;
        tempPR['email'] = '';
        tempPR['id_partner'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/partnerUniversityLogin/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Login ID Not Allowed, Partner Already Registered With The Given Login ID : '+ login_id );
                  $("#login_id").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityEmailIdDuplication()
  {
      var email = $("#email").val()

      if(email != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = '';
        tempPR['login_id'] = '';
        tempPR['email'] = email;
        tempPR['id_partner'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/partnerUniversityLogin/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Email ID Not Allowed, Partner Already Registered With The Given Email ID Id : '+ email );
                  $("#email").val('');
              }
           }
        });
      }
    }

</script>