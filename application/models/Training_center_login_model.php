<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Training_center_login_model extends CI_Model
{
    function loginMe($email, $password)
    {
        $passwordrec = md5($password);
        $this->db->select('usr.*');
        $this->db->from('training_center as usr');
        $this->db->where('usr.email', $email);
        $this->db->where('usr.password', $passwordrec);
        $query = $this->db->get();

        $user = $query->row();
        return $user;
    }

    function editStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }

    function loginAdmin($email, $password)
    {
        $this->db->select('BaseTbl.*, BaseTbl.id as userId, BaseTbl.password, BaseTbl.name, BaseTbl.id_role as roleId, Roles.role');
        $this->db->from('users as BaseTbl');
        $this->db->join('roles as Roles','Roles.id = BaseTbl.role_id','left');
        $this->db->where('BaseTbl.email', $email);
        $query = $this->db->get();
        
        $user = $query->row();

        // echo "<Pre>";print_r($password);exit();
        
        if(!empty($user))
        {
            if($password == $user->password)
            {
                return $user;
            }
            else
            {
                return array();
            }
        }
        else
        {
            return array();
        }

        // return $user;
    }
    

    function trainingCenterLastLoginInfo($training_center_id)
    {
        $this->db->select('usr.created_dt_tm');
        $this->db->where('usr.training_center_id', $training_center_id);
        $this->db->order_by('usr.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('training_center_last_login as usr');

        return $query->row();
    }

    function addTrainingCenterLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('training_center_last_login', $loginInfo);
        $this->db->trans_complete();
    }
}
?>