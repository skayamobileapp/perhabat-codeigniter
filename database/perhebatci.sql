-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 11, 2021 at 01:49 AM
-- Server version: 8.0.26-0ubuntu0.20.04.2
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perhebatci`
--

-- --------------------------------------------------------

--
-- Table structure for table `job_roles`
--

CREATE TABLE `job_roles` (
  `id` bigint NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `status` int DEFAULT '0',
  `created_by` int DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `job_roles`
--

INSERT INTO `job_roles` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'JAS', 1, 1, '2021-11-11 01:39:16', 1, '2021-11-11 01:39:21');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL,
  `parent_name` varchar(200) DEFAULT NULL,
  `order` int DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  `parent_order` int DEFAULT NULL,
  `id_module` varchar(20) DEFAULT NULL,
  `status` int DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES
(1, 'User', 'Setup', 'General Setup', 1, 'user', 'list', 1, '1', 1),
(2, 'Role', 'Setup', 'General Setup', 2, 'role', 'list', 1, '1', 1),
(3, 'Permission', 'Setup', 'General Setup', 3, 'permission', 'list', 1, '1', 1),
(10, 'Training Partner', 'Setup', 'Training', 1, 'trainingPartner', 'list', 2, '1', 1),
(271, 'Training Center', 'Setup', 'Training', 2, 'trainingCenter', 'list', 2, '1', 1),
(272, 'job Roles', 'Setup', 'Training', 3, 'jobRoles', 'list', 2, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint NOT NULL,
  `id_menu` int DEFAULT '0',
  `module` varchar(512) DEFAULT '',
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `id_menu`, `module`, `code`, `description`, `status`) VALUES
(7, 3, 'Setup', 'permission.add', 'Add Permission', 1),
(8, 3, 'Setup', 'permission.list', 'View Permission', 1),
(9, 3, 'Setup', 'permission.edit', 'Edit Permission', 1),
(10, 1, 'Setup', 'user.edit', 'Edit User', 1),
(11, 1, 'Setup', 'user.add', 'Add User', 1),
(12, 1, 'Setup', 'user.list', 'View User', 1),
(13, 2, 'Setup', 'role.list', 'View Role', 1),
(14, 2, 'Setup', 'role.add', 'Add Role', 1),
(15, 2, 'Setup', 'role.edit', 'Edit Role', 1),
(70, 4, 'Setup', 'salutation.list', 'Salutation List', 1),
(71, 4, 'Setup', 'salutation.add', 'Salutation Add', 1),
(72, 4, 'Setup', 'salutation.edit', 'Salutation Edit', 1),
(73, 5, 'Setup', 'country.list', 'Country List', 1),
(74, 5, 'Setup', 'country.add', 'Country Add', 1),
(75, 5, 'Setup', 'country.edit', 'Country Edit', 1),
(76, 6, 'Setup', 'state.list', 'State List', 1),
(77, 7, 'Setup', 'education_level.list', 'Education Level List', 1),
(78, 7, 'Setup', 'education_level.add', 'Education Level Add', 1),
(79, 7, 'Setup', 'education_level.edit', 'Education Level Edit', 1),
(80, 8, 'Setup', 'academic_year.list', 'Academic Year List', 1),
(81, 8, 'Setup', 'academic_year.add', 'Academic Year Add', 1),
(82, 8, 'Setup', 'academic_year.edit', 'Academic Year Edit', 1),
(86, 10, 'Setup', 'race.list', 'Race List', 1),
(87, 10, 'Setup', 'race.add', 'Race Add', 1),
(88, 10, 'Setup', 'race.edit', 'Race Edit', 1),
(89, 11, 'Setup', 'religion.list', 'Religion List', 1),
(90, 11, 'Setup', 'religion.add', 'Religion Add', 1),
(91, 11, 'Setup', 'religion.edit', 'Religion Edit', 1),
(92, 12, 'Setup', 'nationality.list', 'Nationality List', 1),
(93, 12, 'Setup', 'nationality.add', 'Nationality Add', 1),
(94, 12, 'Setup', 'nationality.edit', 'Nationality Edit', 1),
(95, 13, 'Setup', 'department.list', 'Department List', 1),
(96, 13, 'Setup', 'department.add', 'Department Add', 1),
(97, 13, 'Setup', 'department.edit', 'Department Edit', 1),
(98, 14, 'Setup', 'organisation.edit', 'Organisation Edit', 1),
(105, 17, 'Setup', 'faculty_program.list', 'Faculty Program List', 1),
(106, 17, 'Setup', 'faculty_program.add', 'Faculty Program Add', 1),
(107, 17, 'Setup', 'faculty_program.edit', 'Faculty Program Edit', 1),
(108, 18, 'Setup', 'course_description.list', 'course Major / Minor List', 1),
(109, 18, 'Setup', 'course_description.add', 'course Major / Minor Add', 1),
(110, 18, 'Setup', 'course_description.edit', 'course Major / Minor Edit', 1),
(111, 19, 'Setup', 'course_type.list', 'Course Type list', 1),
(112, 19, 'Setup', 'course_type.add', 'Course Type Add', 1),
(113, 19, 'Setup', 'course_type.edit', 'Course Type Edit', 1),
(126, 24, 'Setup', 'scheme.list', 'Scheme List', 1),
(127, 24, 'Setup', 'scheme.add', 'Scheme Add', 1),
(128, 24, 'Setup', 'scheme.edit', 'Scheme Edit', 1),
(153, 33, 'Setup', 'documents_program.list', 'Documents Reuired Program List', 1),
(154, 33, 'Setup', 'documents_program.add', 'Documents Reuired Program Add', 1),
(155, 33, 'Setup', 'documents_program.edit', 'Documents Reuired Program Edit', 1),
(156, 34, 'Setup', 'staff.list', 'Staff List', 1),
(157, 34, 'Setup', 'staff.add', 'Staff Add', 1),
(158, 34, 'Setup', 'staff.edit', 'Staff Edit', 1),
(165, 37, 'Setup', 'report.student_by_intake', 'Report Student By Intake', 1),
(166, 38, 'Setup', 'report.applicant_lead_student', 'Report Applicant Lead Student', 1),
(167, 39, 'Setup', 'report.course_wise_data', 'Report Course Wise Data', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `status`) VALUES
(1, 'Administrator', 1),
(18, 'Account Manager', 1),
(19, 'Product Manager', 1),
(21, 'Recors Manager', 1),
(22, 'Academic Facilitator', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint NOT NULL,
  `id_role` bigint DEFAULT NULL,
  `id_permission` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `training_center`
--

CREATE TABLE `training_center` (
  `id` bigint NOT NULL,
  `training_partner_id` int DEFAULT '0',
  `name` varchar(1024) DEFAULT '',
  `email` varchar(512) DEFAULT '',
  `password` varchar(512) DEFAULT '',
  `mobile` varchar(50) DEFAULT '',
  `status` int DEFAULT '0',
  `created_by` int DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `training_center`
--

INSERT INTO `training_center` (`id`, `training_partner_id`, `name`, `email`, `password`, `mobile`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'as dasdk', 'daskdak@dasd.com', '1e4eda5b9f3ad342a7dcb49db963eb37', '787898', 1, 1, '2021-11-11 01:28:48', 1, '2021-11-11 01:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `training_center_has_job_roles`
--

CREATE TABLE `training_center_has_job_roles` (
  `id` bigint NOT NULL,
  `training_center_id` int DEFAULT '0',
  `job_roles_id` int DEFAULT '0',
  `status` int DEFAULT '0',
  `created_by` int DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `training_partner`
--

CREATE TABLE `training_partner` (
  `id` bigint NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `email` varchar(512) DEFAULT '',
  `contact_person` varchar(512) DEFAULT '',
  `serial_number` varchar(50) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int DEFAULT '0',
  `created_by` int DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `training_partner`
--

INSERT INTO `training_partner` (`id`, `name`, `email`, `contact_person`, `serial_number`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'DASDJ', 'dasndjs@dasd.com', 'dnasdsad', '232131', '', 1, 1, '2021-11-11 00:25:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `role_id` tinyint NOT NULL,
  `id_role` int DEFAULT '0',
  `is_deleted` tinyint NOT NULL DEFAULT '0',
  `image` varchar(512) DEFAULT '',
  `created_by` int NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `mobile`, `role_id`, `id_role`, `is_deleted`, `image`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'admin@gmail.com', '75d23af433e0cea4c0e45a56dba18b30', 'Administrator', '9890098901', 1, 1, 0, '5878a39730c9cb579ad3fe4ad1f3f120.svg', 0, '2015-07-01 18:56:49', 1, '2020-01-30 01:48:49'),
(17, 'records@gmail.com', '6e52c40bb8fc91ff39ee5c79b4211f67', 'Records Person Full Name', '123123123', 21, 0, 0, '', 1, '2021-04-25 21:24:24', 17, '2021-05-10 19:14:50'),
(15, 'product.user@aeu.com', '$2y$10$sBkOeYD4I5osafa3CqHIdO3DofCEZAwhQmvhjtAylyr9ldB2nnor6', 'Product Manager', '890890890', 19, 0, 0, '', 1, '2021-04-25 19:40:49', NULL, NULL),
(14, 'finance@aeu.edu.my', '81dc9bdb52d04dc20036dbd8313ed055', 'Finance User', '12312312312', 18, 0, 0, '', 1, '2021-04-25 18:35:46', NULL, NULL),
(18, 'af@gmail.com', 'f0357a3f154bc2ffe2bff55055457068', 'Af', '123123123', 22, 0, 0, '', 1, '2021-05-04 20:03:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_last_login`
--

CREATE TABLE `user_last_login` (
  `id` bigint NOT NULL,
  `id_user` bigint NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `user_last_login`
--

INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(0, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 95.0.4638.54', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36', 'Linux', ''),
(1, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(2, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(3, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(4, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(5, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(6, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(7, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(8, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(9, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(10, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(11, 1, '{\"role\":\"2\",\"roleText\":\"Ex\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(12, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(13, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(14, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(15, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(16, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(17, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(18, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(19, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(20, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '117.230.180.35', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(21, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(22, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(23, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(24, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(25, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(26, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(27, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(28, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(29, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(30, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(31, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(32, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(33, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(34, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(35, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(36, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(37, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(38, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.159.118', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(39, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(40, 1, '{\"role\":\"1\",\"roleText\":null,\"name\":\"Mr. Virat Kohli\"}', '157.49.104.95', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(41, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.104.95', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(42, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.98.97', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(43, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(44, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(45, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(46, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(47, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(48, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(49, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(50, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(51, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(52, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(53, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(54, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(55, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(56, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(57, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(58, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.160.222', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(59, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.10.50', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(60, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(61, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(62, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.167.29', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(63, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(64, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.165.171', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(65, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(66, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.172.50', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(67, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(68, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.167.25', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(69, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.170.70', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(70, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.171.206', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(71, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(72, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '106.206.63.4', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(73, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(74, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.26.2', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(75, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '223.237.199.118', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(76, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(77, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(78, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(79, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(80, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(81, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(82, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(83, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.157.79', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(84, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(85, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(86, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(87, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(88, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(89, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(90, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(91, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '106.217.109.107', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(92, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(93, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(94, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(95, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '223.186.95.105', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(96, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(97, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(98, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(99, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.172.230', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(100, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(101, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(102, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(103, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(104, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.8.244', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(105, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(106, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(107, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(108, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.109.211', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(109, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(110, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.18.147', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(111, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.18.147', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(112, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(113, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(114, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(115, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.196.167', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(116, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.166', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(117, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.205.174', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(118, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.166', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(119, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.201.119', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(120, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(121, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(122, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.193.89', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(123, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(124, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.211.232', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(125, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.145.115', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(126, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(127, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(128, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.124.132', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(129, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.209.249', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(130, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.233.127', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(131, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(132, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(133, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(134, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(135, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(136, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(137, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.237.160', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(138, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.18.147', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(139, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(140, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(141, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(142, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.186.122', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(143, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.164.222', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(144, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(145, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(146, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.254.227', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(147, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '106.206.60.221', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(148, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.138.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(149, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(150, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(151, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.40', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(152, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.80.129', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(153, 13, '{\"role\":\"17\",\"roleText\":\"Finance Administrator\",\"name\":\"Finance User\"}', '157.45.80.129', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(154, 13, '{\"role\":\"17\",\"roleText\":\"Finance Administrator\",\"name\":\"Finance User\"}', '157.45.80.129', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(155, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.80.129', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(156, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.78.149', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(157, 13, '{\"role\":\"17\",\"roleText\":\"Finance Administrator\",\"name\":\"Finance User\"}', '157.45.78.149', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(158, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(159, 13, '{\"role\":\"17\",\"roleText\":\"Finance Administrator\",\"name\":\"Finance User\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(160, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(161, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.67.41', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(162, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.64.116', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(163, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.73.193', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(164, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.61', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(165, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(166, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.10.161', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(167, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.10.161', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(168, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.85.131', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(169, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.85.131', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(170, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.85.131', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(171, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.85.131', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(172, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.124.154', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(173, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.91.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(174, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.91.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(175, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.59.160', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(176, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.143.10', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(177, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(178, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(179, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.146.133', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(180, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.131.109', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(181, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(182, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.128.46', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08');
INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(183, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(184, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(185, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(186, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.151.145', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(187, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(188, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(189, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.159.145', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(190, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(191, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(192, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(193, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.130.146', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(194, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(195, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(196, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.131.206', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(197, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(198, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(199, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(200, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(201, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(202, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(203, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.175.71', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(204, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 90.0.4430.85', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(205, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 90.0.4430.85', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(206, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.148.31', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(207, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(208, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.161.193', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(209, 17, '{\"role\":\"21\",\"roleText\":\"Recors Manager\",\"name\":\"Records Manager\"}', '157.45.161.193', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2020-08-08 08:08:08'),
(210, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.152.183', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(211, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(212, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(213, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(214, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(215, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.25.64', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(216, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.157.17', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(217, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.11.33', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(218, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(219, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(220, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(221, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.30.32', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(222, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.30.32', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(223, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(224, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.19.116', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(225, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(226, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(227, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.0.25', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(228, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(229, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(230, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(231, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(232, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 90.0.4430.85', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36 Edg/90.0.818.49', 'Windows 10', '2020-08-08 08:08:08'),
(233, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(234, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(235, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.186.88', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(236, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.185.57', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(237, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(238, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(239, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"\",\"name\":\"Administrator\"}', '157.45.183.133', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(240, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '157.45.183.133', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(241, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(242, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '157.45.27.195', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(243, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '157.45.140.198', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(244, 18, '{\"role\":\"22\",\"roleText\":\"Academic Facilitator\",\"user_image\":\"\",\"name\":\"Af\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(245, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(246, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.23.255', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(247, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(248, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(249, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.15.65', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(250, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.15.65', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(251, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(252, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '117.230.11.189', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(253, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '117.230.50.141', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(254, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.4.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(255, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.4.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(256, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.4.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(257, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(258, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.9.103', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(259, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(260, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(261, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(262, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.28.79', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(263, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.14.121', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(264, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(265, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.5.238', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(266, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(267, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.14.8', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(268, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(269, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.11.154', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(270, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(271, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Windows 10', '2020-08-08 08:08:08'),
(272, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '117.230.128.171', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(273, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(274, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.18.106', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(275, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(276, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '27.59.165.147', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Mac OS X', '2020-08-08 08:08:08'),
(277, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '27.59.165.147', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Mac OS X', '2020-08-08 08:08:08'),
(278, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(279, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(280, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(281, 17, '{\"role\":\"21\",\"roleText\":\"Recors Manager\",\"user_image\":\"\",\"name\":\"Records Manager\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(282, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-08 08:08:08'),
(283, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.120.218', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(284, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.120.218', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2020-08-08 08:08:08'),
(285, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.120.218', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-05-08 08:08:08'),
(286, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.241.238', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-05-08 08:08:08'),
(287, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.241.238', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(288, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '27.59.177.149', 'Chrome 90.0.4430.212', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'Mac OS X', ''),
(289, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.251.228', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(290, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.149.100', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(291, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.149.100', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(292, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.121.50', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(293, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(294, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(295, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.240.103', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(296, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.173.160', 'Chrome 90.0.4430.212', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'Windows 10', ''),
(297, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.117.61', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(298, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(299, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(300, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '117.230.39.230', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(301, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.50.41.190', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(302, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.124.78', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(303, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(304, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.212', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'Windows 10', ''),
(305, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(306, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.212', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'Windows 10', ''),
(307, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.131.197', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(308, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.132.244', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', ''),
(309, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(310, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(311, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(312, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.212', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'Windows 10', ''),
(313, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(314, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 91.0.4472.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'Windows 10', ''),
(315, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 91.0.4472.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'Windows 10', ''),
(316, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 91.0.4472.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'Windows 10', ''),
(317, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(318, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 91.0.4472.77', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'Windows 10', ''),
(319, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(320, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.101', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36', 'Windows 10', ''),
(321, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(322, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(323, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', ''),
(324, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.117.174', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', ''),
(325, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.8.160', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Windows 10', ''),
(326, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.51.82.40', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(327, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.51.82.40', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(328, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(329, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'Windows 10', ''),
(330, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'Windows 10', ''),
(331, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'Windows 10', ''),
(332, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'Windows 10', ''),
(333, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'Windows 10', ''),
(334, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'Windows 10', ''),
(335, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(336, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.51.82.40', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(337, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Windows 10', ''),
(338, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '175.137.114.68', 'Chrome 91.0.4472.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', 'Windows 10', ''),
(339, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.161.154', 'Chrome 91.0.4472.124', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'Windows 10', ''),
(340, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(341, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(342, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(343, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.200.198.200', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(344, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.51.82.40', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(345, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.193.105', 'Chrome 92.0.4515.131', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', 'Windows 10', ''),
(346, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', '');
INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(347, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(348, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(349, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.193.105', 'Chrome 92.0.4515.159', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', 'Windows 10', ''),
(350, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.193.105', 'Chrome 92.0.4515.159', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', 'Windows 10', ''),
(351, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(352, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(353, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.51.82.40', 'Chrome 91.0.4472.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'Linux', ''),
(354, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.193.105', 'Chrome 92.0.4515.159', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', 'Windows 10', ''),
(355, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.193.105', 'Chrome 92.0.4515.159', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', 'Windows 10', ''),
(356, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.178.122', 'Chrome 93.0.4577.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', 'Windows 10', ''),
(357, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.223.83', 'Chrome 95.0.4638.54', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36', 'Windows 10', ''),
(358, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.51.82.40', 'Chrome 94.0.4606.81', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', 'Linux', ''),
(359, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.51.82.40', 'Chrome 94.0.4606.81', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', 'Linux', ''),
(360, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.223.83', 'Chrome 95.0.4638.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 'Windows 10', ''),
(361, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '106.51.82.40', 'Chrome 94.0.4606.81', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', 'Linux', ''),
(362, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.223.83', 'Chrome 95.0.4638.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 'Windows 10', ''),
(363, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 94.0.4606.81', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', 'Linux', ''),
(364, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 94.0.4606.81', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', 'Linux', ''),
(365, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '219.92.223.83', 'Chrome 95.0.4638.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 'Windows 10', ''),
(366, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 95.0.4638.54', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36', 'Linux', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `job_roles`
--
ALTER TABLE `job_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_center`
--
ALTER TABLE `training_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_center_has_job_roles`
--
ALTER TABLE `training_center_has_job_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_partner`
--
ALTER TABLE `training_partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_last_login`
--
ALTER TABLE `user_last_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `job_roles`
--
ALTER TABLE `job_roles`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=598;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6388;

--
-- AUTO_INCREMENT for table `training_center`
--
ALTER TABLE `training_center`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `training_center_has_job_roles`
--
ALTER TABLE `training_center_has_job_roles`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_partner`
--
ALTER TABLE `training_partner`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user_last_login`
--
ALTER TABLE `user_last_login`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=367;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
