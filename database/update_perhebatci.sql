
CREATE TABLE `training_partner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `email` varchar(512) DEFAULT '',
  `contact_person` varchar(512) DEFAULT '',
  `serial_number` varchar(50) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `training_center` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `training_partner_id` int(20) DEFAULT '0',
  `name` varchar(1024) DEFAULT '',
  `email` varchar(512) DEFAULT '',
  `password` varchar(512) DEFAULT '',
  `mobile` varchar(50) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `job_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;






CREATE TABLE `training_center_has_job_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `training_center_id` int(20) DEFAULT '0',
  `job_roles_id` int(20) DEFAULT '0',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Student', 'Setup', 'Student', '3', 'student', 'list', '3', '1', '1');


CREATE TABLE `student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `full_name` varchar(512) DEFAULT '',
  `first_name` varchar(512) DEFAULT '',
  `last_name` varchar(512) DEFAULT '',
  `contact_number` varchar(50) DEFAULT '',
  `email` varchar(512) DEFAULT '',
  `password` varchar(512) DEFAULT '',
  `gender` varchar(512) DEFAULT '',
  `dob` varchar(512) DEFAULT '',
  `father_name` varchar(512) DEFAULT '',
  `bank_name` varchar(512) DEFAULT '',
  `account_number` varchar(512) DEFAULT '',
  `highest_education` varchar(512) DEFAULT '',
  `year_of_passing` varchar(512) DEFAULT '',
  `address` varchar(512) DEFAULT '',
  `address_two` varchar(512) DEFAULT '',
  `zipcode` varchar(512) DEFAULT '',
  `country_id` int(20) DEFAULT '0',
  `state_id` int(20) DEFAULT '0',
  `training_center_id` int(20) DEFAULT '0',
  `job_role_id` int(20) DEFAULT '0',
  `status` varchar(512) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `training_center_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `training_center_id` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Profile', 'training_center', 'Training Center', '1', 'profile', 'index', '1', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Sudent', 'training_center', 'Training Center', '2', 'student', 'index', '1', '1', '1');

------------------ Update -------------------------

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Pending Student', 'Setup', 'Student', '4', 'studentPending', 'list', '3', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Approved Student', 'Setup', 'Student', '5', 'studentApproved', 'list', '3', '1', '1');






